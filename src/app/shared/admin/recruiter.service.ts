import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecruiterService {

  constructor(private _httpClient: HttpClient) { }

  public baseUrl = environment.BASE_URL;

  public publicHeader() {
    let httpOptionsSecure = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return httpOptionsSecure;
  }

  public secureHeader() {
    let httpOptionsSecure = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization:
          'Bearer ' +
          (
            localStorage.getItem('token')
          ).trim()
      })
    };
    return httpOptionsSecure;
  }

  onGetRecruiters(): Observable<any> {
    return this._httpClient.get(this.baseUrl + 'admin/getallrecruiters', this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onDeleteRecruiter(id): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'admin/deleterecruiter', { id: id }, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onGetCities(): Observable<any> {
    return this._httpClient.get(this.baseUrl + 'recruiter/getcities', this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onAddRecruiter(recruiterObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'admin/addrecruiter', recruiterObj, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onGetRecruiter(recruiterObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'admin/getrecruiter', recruiterObj, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onUpdateRecruiter(recruiterObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'admin/updaterecruiter', recruiterObj, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

}
