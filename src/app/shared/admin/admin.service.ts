import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private _httpClient: HttpClient) { }

  public baseUrl = environment.BASE_URL;

  public publicHeader() {
    let httpOptionsSecure = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return httpOptionsSecure;
  }

  public secureHeader() {
    let httpOptionsSecure = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization:
          'Bearer ' +
          (
            localStorage.getItem('token')
          ).trim()
      })
    };
    return httpOptionsSecure;
  }

  onLoginAdmin(user): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'admin/login', user, this.publicHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
        })
      );
  }

}
