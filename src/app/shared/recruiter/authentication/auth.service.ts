import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public jwtHelper: JwtHelperService) { }
  
  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');   
    if (token) {
      // const helper = new JwtHelperService();
      // const decoded = helper.decodeToken(token);
      // console.log(this.jwtHelper.isTokenExpired(token));
      // console.log(token)
      return true;
    }    
  }
  
}
