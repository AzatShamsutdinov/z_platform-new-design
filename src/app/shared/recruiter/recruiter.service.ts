import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecruiterService {

  constructor(private _httpClient: HttpClient) { }

  public baseUrl = environment.BASE_URL;

  public publicHeader() {
    let httpOptionsSecure = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return httpOptionsSecure;
  }

  public secureHeader() {
    let httpOptionsSecure = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization:
          'Bearer ' +
          (
            localStorage.getItem('token')
          ).trim()
      })
    };
    return httpOptionsSecure;
  }

  onLoginRecruiter(user): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/login', user, this.publicHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }
  onSignUpRecruiter(user): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/recruitersignup', user, this.publicHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }
  onForgotPassword(email): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/sendEmailVerificationCode', email, this.publicHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }
  onVerifyEmail(id,token,data): Observable<any> {
    return this._httpClient.post(this.baseUrl + `recruiter/verfiyEmailVerificationCode/${id}/${token}`,data, this.publicHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  getUsers(): Observable<any> {
    return this._httpClient.get(this.baseUrl + 'recruiter/getusers', this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  getskills(): Observable<any> {
    return this._httpClient.get(this.baseUrl + 'recruiter/getskills', this.secureHeader())
  }

}
