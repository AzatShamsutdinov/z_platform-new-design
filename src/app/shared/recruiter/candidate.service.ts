import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { error } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class CandidateService {

  constructor(private _httpClient: HttpClient) { }

  public baseUrl = environment.BASE_URL;

  public publicHeader() {
    let httpOptionsSecure = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return httpOptionsSecure;
  }

  public secureHeader() {
    let httpOptionsSecure = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization:
          'Bearer ' +
          (
            localStorage.getItem('token')
          ).trim()
      })
    };
    return httpOptionsSecure;
  }

  onGetCandidates(): Observable<any> {
    return this._httpClient.get(this.baseUrl + 'recruiter/getallcandidates', this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onDeleteCandidate(id): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/deletecandidate', { id: id }, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onAddCandidate(candidateObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/addcandidate', candidateObj, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onGetCities(): Observable<any> {
    return this._httpClient.get(this.baseUrl + 'recruiter/getcities', this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onGetSkills(): Observable<any> {
    return this._httpClient.get(this.baseUrl + 'recruiter/getskills', this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  getScores(): Observable<any> {
    return this._httpClient.get(this.baseUrl + 'recruiter/getscores', this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error)
      }));
  }

  onGetCandidate(candidateObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/getcandidate', candidateObj, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onUpdateCandidate(candidateObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/updatecandidate', candidateObj, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onShowInterview(interviewObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/interviewanswer', interviewObj, this.publicHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onGetComments(commentObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/getcomments', commentObj, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error)
      })
      );
  }

  onAddComment(commentobj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/addcomments', commentobj, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error)
      })
      );
  }

  onGetAddedComments(getComments): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/getaddedcomments', getComments, this.secureHeader())
      .pipe(map(res=>res), catchError(error => {
        return throwError(error)
      })
      );  
  }

}
