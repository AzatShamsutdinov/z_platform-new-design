import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VacancyService {

  constructor(private _httpClient: HttpClient) { }

  public baseUrl = environment.BASE_URL;

  public publicHeader() {
    let httpOptionsSecure = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return httpOptionsSecure;
  }

  public secureHeader() {
    let httpOptionsSecure = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization:
          'Bearer ' +
          (
            localStorage.getItem('token')
          ).trim()
      })
    };
    return httpOptionsSecure;
  }

  onGetVacancies(): Observable<any> {
    return this._httpClient.get(this.baseUrl + 'recruiter/getallvacancies', this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onAddVacancies(vacancyObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/addvacancy', vacancyObj, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onGetCities(): Observable<any> {
    return this._httpClient.get(this.baseUrl + 'recruiter/getcities', this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onDeleteVacancy(id): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/deletevacancy', { id: id }, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onGetVacancy(vacancyObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/getvacancy', vacancyObj, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onUpdateVacancy(vacancyObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/updatevacancy', vacancyObj, this.secureHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  getQuestionFromSkills(noOfSkills) : Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/getquestionfromskills', noOfSkills, this.secureHeader())
    .pipe(map(res=>res), catchError( error => {
      return throwError(error)
    }))
  }
  
  getQuestionFromId(quesId) : Observable<any> {
    return this._httpClient.post(this.baseUrl + 'recruiter/getquestionfromid', quesId, this.secureHeader())
    .pipe(map(res=>res), catchError( error => {
      return throwError(error)
    }))
  }
}
