import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CandidateService {

  constructor(private _httpClient: HttpClient) { }

  public baseUrl = environment.BASE_URL;

  public publicHeader() {
    let httpOptionsSecure = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return httpOptionsSecure;
  }
  
  public fileuploadHeader() {
    let httpOptionsSecure = {
      headers: new HttpHeaders({
        
      })
    };
    return httpOptionsSecure;
  }

  onGetQuestion(questionObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'candidate/questionsByLink', questionObj, this.publicHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onCandidateDetails(candidate): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'candidate/checkdetails', candidate, this.publicHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
        })
      );
  }
  
  onSendAnswerFile(answerObj): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'candidate/answerFiles', answerObj, this.fileuploadHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
      })
      );
  }

  onSendAnswer(candidate): Observable<any> {
    return this._httpClient.post(this.baseUrl + 'candidate/questionanswer', candidate, this.publicHeader())
      .pipe(map(res => res), catchError(error => {
        return throwError(error);
        })
      );
  }

}
