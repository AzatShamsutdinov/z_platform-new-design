import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { AddRecruiterComponent } from './admin/recruiter/add-recruiter/add-recruiter.component';
import { EditRecruiterComponent } from './admin/recruiter/edit-recruiter/edit-recruiter.component';
import { RecruiterComponent } from './admin/recruiter/recruiter.component';
import { BaseComponent } from './base/base.component';
import { CheckEquipmentComponent } from './candidate/check-equipment/check-equipment.component';
import { DetailsComponent } from './candidate/details/details.component';
import { InterviewQuestionComponent } from './candidate/interview-question/interview-question.component';
import { ThankYouComponent } from './candidate/thank-you/thank-you.component';
import { AddCandidateComponent } from './recruiter/candidate/add-candidate/add-candidate.component';
import { CandidateComponent } from './recruiter/candidate/candidate.component';
import { EditCandidateComponent } from './recruiter/candidate/edit-candidate/edit-candidate.component';
import { RecruiterDashboardComponent } from './recruiter/recruiter-dashboard/recruiter-dashboard.component';
import { RecruiterLoginComponent } from './recruiter/recruiter-login/recruiter-login.component';
import { AddVacancyComponent } from './recruiter/vacancy/add-vacancy/add-vacancy.component';
import { EditVacancyComponent } from './recruiter/vacancy/edit-vacancy/edit-vacancy.component';
import { QuestionComponent } from './recruiter/vacancy/question/question.component';
import { VacancyComponent } from './recruiter/vacancy/vacancy.component';
import { AuthGuardService as AuthGuardLogin } from './shared/admin/authentication/auth-guard.service';
import { AuthGuardService as AuthGuardRecruiter } from './shared/recruiter/authentication/auth-guard.service';
import { AuthGuardService as AuthGuardCandidate } from './shared/candidate/authentication/auth-guard.service';
import { ViewInterviewComponent } from './recruiter/view-interview/view-interview.component';
import { TermsConditionsComponent } from './candidate/terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './candidate/privacy-policy/privacy-policy.component';
import { InterviewComponent } from './recruiter/interview/interview.component';
import { RecruiterForgotpasswordComponent } from "./recruiter/recruiter-forgotpassword/recruiter-forgotpassword.component";
import { RecruiterNewpasswordComponent } from "./recruiter/recruiter-newpassword/recruiter-newpassword.component";
import { RecruiterSignupComponent } from './recruiter/recruiter-signup/recruiter-signup.component';
const routes: Routes = [
  { path: '', component: BaseComponent },
  { path: 'admin/login', component: AdminLoginComponent },
  { path: 'admin/dashboard', component: AdminDashboardComponent, canActivate: [AuthGuardLogin], data: {} },
  { path: 'admin/recruiter', component: RecruiterComponent, canActivate: [AuthGuardLogin], data: {} },
  { path: 'admin/add-recruiter', component: AddRecruiterComponent, canActivate: [AuthGuardLogin], data: {} },
  { path: 'admin/edit-recruiter/:id', component: EditRecruiterComponent },

  { path: 'recruiter/login', component: RecruiterLoginComponent },
  { path: 'recruiter/signup', component: RecruiterSignupComponent },
  { path: 'recruiter/newpassword/:id/:token', component: RecruiterNewpasswordComponent },
  { path: 'recruiter/forgotpassword', component: RecruiterForgotpasswordComponent },
  { path: 'recruiter/dashboard', component: RecruiterDashboardComponent, canActivate: [AuthGuardRecruiter], data: {} },
  { path: 'recruiter/vacancy', component: VacancyComponent, canActivate: [AuthGuardRecruiter], data: {} },
  { path: 'recruiter/add-vacancy', component: AddVacancyComponent, canActivate: [AuthGuardRecruiter], data: {} },
  { path: 'recruiter/edit-vacancy/:id', component: EditVacancyComponent, canActivate: [AuthGuardRecruiter], data: {} },
  { path: 'recruiter/candidate', component: CandidateComponent, canActivate: [AuthGuardRecruiter], data: {} },
  { path: 'recruiter/add-candidate', component: AddCandidateComponent, canActivate: [AuthGuardRecruiter], data: {} },
  { path: 'recruiter/edit-candidate/:id', component: EditCandidateComponent, canActivate: [AuthGuardRecruiter], data: {} },
  { path: 'recruiter/question/:id', component: QuestionComponent, canActivate: [AuthGuardRecruiter], data: {} },
  { path: 'recruiter/view-interview/:id', component: ViewInterviewComponent, canActivate: [AuthGuardRecruiter], data: {} },
  { path: 'recruiter/interview/:id', component: InterviewComponent, data: {} },
  { path: 'candidate/details/:code', component: DetailsComponent },
  { path: 'candidate/check-equipment/:code', component: CheckEquipmentComponent, canActivate: [AuthGuardCandidate], data: {} },
  { path: 'candidate/question/:code', component: InterviewQuestionComponent, canActivate: [AuthGuardCandidate], data: {} },
  { path: 'candidate/thank-you', component: ThankYouComponent },
  { path: 'candidate/terms-conditions', component: TermsConditionsComponent },
  { path: 'candidate/privacy-policy', component: PrivacyPolicyComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
