import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class AdminHeaderComponent implements OnInit {

  admin = '';

  constructor(private _router: Router) { }

  onAdminLogout() {
    if(confirm('Are you sure you want to logged out ?')) {
      localStorage.clear();
      this._router.navigate(['/admin/login']);
    }
  }

  ngOnInit(): void {
    this.admin = localStorage.getItem('admin');
  }

}
