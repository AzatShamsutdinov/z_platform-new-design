import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RecruiterService } from 'src/app/shared/admin/recruiter.service';

@Component({
  selector: 'app-add-recruiter',
  templateUrl: './add-recruiter.component.html',
  styleUrls: ['./add-recruiter.component.css']
})
export class AddRecruiterComponent implements OnInit {

  recruiterObj: any = {
    username: '',
    fullname: '',
    phone: '',
    email: '',
    password: '',
    company: '',
    city: ''
  };
  // cities: any = {};
  successMsg = '';

  constructor(private _router: Router,
              private _recruiterService: RecruiterService,
              private _title: Title) { }

  onCancel() {
    this._router.navigate(['/admin/recruiter']);
  }

  // onGetCity() {
  //   this._recruiterService.onGetCities().subscribe(data => {
  //     let result: any = data;
  //     if (result.success == true) {
  //       this.cities = result.data;
  //       console.log(this.cities);
  //     }
  //   }, (err) => {
  //     if (err.status == 400) {
  //       console.log(err.error.msg);
  //     }
  //   })
  // }

  onAddRecruiter(form: NgForm) {
    console.log(form)
    console.log(this.recruiterObj)
    this._recruiterService.onAddRecruiter(this.recruiterObj).subscribe(data => {
      if (data.success == true) {
        // this._toastr.success('Successfully registered. Please login to continue.', 'Success', {
        //   timeOut: 2000,
        //   positionClass: 'toast-center-center',
        //   progressBar: true,
        //   progressAnimation: 'increasing'
        // });
        this.successMsg = 'Recruiter added successfully.';
        form.resetForm();
      }
    }, e => {
      if (e.status == 400) {
        // this._toastr.error(e.error.msg, 'Error', {
        //   timeOut: 2000,
        //   positionClass: 'toast-center-center',
        //   progressBar: true,
        //   progressAnimation: 'increasing'
        // });
        console.log('Error');
      }
    })
  }

  ngOnInit(): void {
    this._title.setTitle('Recruiter - Add');
    // this.onGetCity();
  }

}
