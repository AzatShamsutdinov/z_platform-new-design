import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RecruiterService } from 'src/app/shared/admin/recruiter.service';

@Component({
  selector: 'app-recruiter',
  templateUrl: './recruiter.component.html',
  styleUrls: ['./recruiter.component.css']
})
export class RecruiterComponent implements OnInit {

  recruiters: any[];
  result: any = {};
  deleteSucc = '';

  constructor(private _router: Router,
              private _recruiterService: RecruiterService,
              private _title: Title) { }

  onAdd() {
    this._router.navigate(['admin/add-recruiter']);
  }

  onEdit(id) {
    this._router.navigate(['/admin/edit-recruiter', id]);
  }

  onGetRecruiters() {
    this._recruiterService.onGetRecruiters().subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.recruiters = result.data;
        console.log(this.recruiters);
      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })
  }

  onDeleteRecruiter(id) {
    if(confirm('Are you sure you want to delete this recruiter ?')) {
      this._recruiterService.onDeleteRecruiter(id).subscribe(data => {
        let result: any = data;
        if (result.success) {
          this.onGetRecruiters();
          this.deleteSucc = 'Recruiter deleted successfully.'
        }
      }, err => {
        console.log(err.msg);
      })
    }    
  }  

  ngOnInit(): void {
    this._title.setTitle('Recruiters');
    this.onGetRecruiters();
  }

}
