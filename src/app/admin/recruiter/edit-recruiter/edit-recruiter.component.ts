import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { RecruiterService } from 'src/app/shared/admin/recruiter.service';

@Component({
  selector: 'app-edit-recruiter',
  templateUrl: './edit-recruiter.component.html',
  styleUrls: ['./edit-recruiter.component.css']
})
export class EditRecruiterComponent implements OnInit {

  recruiterId: any;
  recruiters: any = {};
  // cities: any = {};
  recruiterObj: any = {
    fullname: '',
    phone: '',
    email: '',
    company: '',
    city: '',
    status: '',
    id: ''
  };
  recruiterIdObj: any = {
    id: ''
  };
  updateMsg = '';

  constructor(private _router: Router,
              private _recruiterService: RecruiterService,
              private _route: ActivatedRoute,
              private _title: Title) { }

  onGetRecruiter(id) {
    this.recruiterIdObj.id=id;
    this._recruiterService.onGetRecruiter(this.recruiterIdObj).subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.recruiterObj.fullname = result.data[0].FULLNAME;
        this.recruiterObj.phone = result.data[0].PHONE;
        this.recruiterObj.email = result.data[0].EMAIL;
        this.recruiterObj.company = result.data[0].COMPANY;
        this.recruiterObj.city = result.data[0].CITY;
        this.recruiterObj.status = result.data[0].STATUS;
        this.recruiterObj.id = result.data[0].ID;
        console.log(result.data);
      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })
  }

  // onGetCity() {
  //   this._recruiterService.onGetCities().subscribe(data => {
  //     let result: any = data;
  //     if (result.success == true) {
  //       this.cities = result.data;
  //       console.log(this.cities);
  //     }
  //   }, (err) => {
  //     if (err.status == 400) {
  //       console.log(err.error.msg);
  //     }
  //   })
  // }

  onUpdateRecruiter(form: NgForm) {    
    this._recruiterService.onUpdateRecruiter(this.recruiterObj).subscribe(data => {
      let result: any = data;
      if (result.success) {
        this.updateMsg = 'Recruiter updated successfully.'
      }
    }, err => {
      console.log(err.msg);
    })
  }

  onCancel() {
    this._router.navigate(['/admin/recruiter']);
  }

  ngOnInit(): void {
    this._title.setTitle('Recruiter - Edit');
    this.recruiterId=this._route.snapshot.paramMap.get('id');
    this.onGetRecruiter(this.recruiterId);
    // this.onGetCity();
  }

}
