import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/shared/admin/admin.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  loginObj: any = {
    username: '',
    password: ''
  };
  result: any = {};
  loginFail = '';

  constructor(private _adminService: AdminService,
              private _router: Router,
              private _title: Title) { }
  
  onLogin(form: NgForm) {
    console.log(this.loginObj)
    if (this.loginObj) {
      this._adminService.onLoginAdmin(this.loginObj).subscribe(data => {
        this.result = data;
        console.log(data);
        if (this.result.success == true) {
          // console.log(data);
          //this._toastr.success('Log in successful.', 'Success');
          
          localStorage.setItem('admin', this.result.admin);
          localStorage.setItem('token', this.result.token);
          this._router.navigateByUrl("/admin/dashboard");
        }
      }, (err) => {
        if (err.status == 400) {
          // this._toastr.error(err.error.msg);
          this.loginFail = 'Please check your username and password !';
        }
      })
    }
  }

  onCancel() {
    this._router.navigate(['/']);
  }

  ngOnInit(): void {
    this._title.setTitle('Admin - Login');
  }

}
