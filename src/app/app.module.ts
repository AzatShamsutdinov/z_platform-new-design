import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BaseComponent } from './base/base.component';
import { AdminLoginComponent } from './admin/admin-login/admin-login.component';
import { AdminDashboardComponent } from './admin/admin-dashboard/admin-dashboard.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminHeaderComponent } from './admin/layout/admin-header/admin-header.component';
import { AdminFooterComponent } from './admin/layout/admin-footer/admin-footer.component';
import { JwtModule } from '@auth0/angular-jwt';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RecruiterLoginComponent } from './recruiter/recruiter-login/recruiter-login.component';
import { RecruiterDashboardComponent } from './recruiter/recruiter-dashboard/recruiter-dashboard.component';
import { RecruiterHeaderComponent } from './recruiter/layout/recruiter-header/recruiter-header.component';
import { RecruiterFooterComponent } from './recruiter/layout/recruiter-footer/recruiter-footer.component';
import { VacancyComponent } from './recruiter/vacancy/vacancy.component';
import { AddVacancyComponent } from './recruiter/vacancy/add-vacancy/add-vacancy.component';
import { EditVacancyComponent } from './recruiter/vacancy/edit-vacancy/edit-vacancy.component';
import { CandidateComponent } from './recruiter/candidate/candidate.component';
import { AddCandidateComponent } from './recruiter/candidate/add-candidate/add-candidate.component';
import { EditCandidateComponent } from './recruiter/candidate/edit-candidate/edit-candidate.component';
import { RecruiterComponent } from './admin/recruiter/recruiter.component';
import { AddRecruiterComponent } from './admin/recruiter/add-recruiter/add-recruiter.component';
import { EditRecruiterComponent } from './admin/recruiter/edit-recruiter/edit-recruiter.component';
import { QuestionComponent } from './recruiter/vacancy/question/question.component';
import { DetailsComponent } from './candidate/details/details.component';
import { CheckEquipmentComponent } from './candidate/check-equipment/check-equipment.component';
import { InterviewQuestionComponent } from './candidate/interview-question/interview-question.component';
import { CountdownModule } from 'ngx-countdown';
import { NgxPaginationModule } from 'ngx-pagination';
import { ThankYouComponent } from './candidate/thank-you/thank-you.component';
import { ViewInterviewComponent } from './recruiter/view-interview/view-interview.component';
import { ClipboardModule } from 'ngx-clipboard';
import { TermsConditionsComponent } from './candidate/terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './candidate/privacy-policy/privacy-policy.component';
import { InterviewComponent } from './recruiter/interview/interview.component';
import { AuthInterceptorService } from './shared/auth-interceptor.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { RecruiterForgotpasswordComponent } from './recruiter/recruiter-forgotpassword/recruiter-forgotpassword.component';
import { RecruiterNewpasswordComponent } from './recruiter/recruiter-newpassword/recruiter-newpassword.component';
import { AddQuestionComponent } from './recruiter/vacancy/add-question/add-question.component';
import { RecruiterSignupComponent } from './recruiter/recruiter-signup/recruiter-signup.component';
import { Ng2SearchPipeModule } from "ng2-search-filter";

export function jwtTokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    AdminLoginComponent,
    AdminDashboardComponent,
    AdminHeaderComponent,
    AdminFooterComponent,
    RecruiterLoginComponent,
    RecruiterDashboardComponent,
    RecruiterHeaderComponent,
    RecruiterFooterComponent,
    VacancyComponent,
    AddVacancyComponent,
    EditVacancyComponent,
    CandidateComponent,
    AddCandidateComponent,
    EditCandidateComponent,
    RecruiterComponent,
    AddRecruiterComponent,
    EditRecruiterComponent,
    QuestionComponent,
    DetailsComponent,
    CheckEquipmentComponent,
    InterviewQuestionComponent,
    ThankYouComponent,
    ViewInterviewComponent,
    TermsConditionsComponent,
    PrivacyPolicyComponent,
    InterviewComponent,
    RecruiterForgotpasswordComponent,
    RecruiterNewpasswordComponent,
    AddQuestionComponent,
    RecruiterSignupComponent
  ],
  imports: [
    BrowserModule,
    CountdownModule,
    NgxPaginationModule,
    AppRoutingModule,
    HttpClientModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    FormsModule,
    ClipboardModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,

    JwtModule.forRoot({
      config: {
        tokenGetter: jwtTokenGetter
      }
    })
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }



// Add # in url
// { provide: LocationStrategy, useClass: HashLocationStrategy }