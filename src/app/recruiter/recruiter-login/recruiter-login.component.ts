import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormsModule, NgModel } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RecruiterService } from 'src/app/shared/recruiter/recruiter.service';

@Component({
  selector: 'app-recruiter-login',
  templateUrl: './recruiter-login.component.html',
  styleUrls: ['./recruiter-login.component.css']
})
export class RecruiterLoginComponent implements OnInit {

  loginObj: any = {
    username: '',
    password: ''
  };
  result: any = {};
  loginFail = '';
  showPassword = false;
  passwordType = 'password'

  @ViewChild('password') password: ElementRef<HTMLInputElement>;

  constructor(private _recruiterService: RecruiterService,
    private _router: Router,
    private _title: Title) {


  }
  onInputChange(e){
    if(e.target.value != ''){
      this.loginFail = ''
    }
  }
  clickCheckbox(id) {
    document.getElementById(id).click()
  }
  onSignUp() {
    this._router.navigate(['/recruiter/signup']);
  }
  forgotPassword() {
    this._router.navigate(['/recruiter/forgotpassword']);
  }
  onLogin(form: NgForm) {
    console.log(this.loginObj)
    if (this.loginObj) {
      this._recruiterService.onLoginRecruiter(this.loginObj).subscribe(data => {
        this.result = data;
        console.log(data);
        if (this.result.success == true) {
          // console.log(data);
          //this._toastr.success('Log in successful.', 'Success');

          localStorage.setItem('recruiter', this.result.recruiter);
          localStorage.setItem('uid', this.result.uid);
          localStorage.setItem('token', this.result.token);
          this._router.navigateByUrl("/recruiter/dashboard");
        }
      }, (err) => {
        if (err.status == 400) {
          // this._toastr.error(err.error.msg);
          this.loginFail = 'Invalid username or password';
        }
      })
    }
  }
  
  onCancel() {
    this._router.navigate(['/']);
  }

  ngOnInit(): void {
    this._title.setTitle('Recruiter - Login');
  }

  toggleShow() {
    this.showPassword = !this.showPassword
    if (!this.showPassword) {
      this.passwordType = 'password'
    }
    else {
      this.passwordType = 'text'
    }
  }
}
