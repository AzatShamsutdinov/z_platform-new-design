import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormsModule, NgModel } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RecruiterService } from 'src/app/shared/recruiter/recruiter.service';

@Component({
  selector: 'app-recruiter-signup',
  templateUrl: './recruiter-signup.component.html',
  styleUrls: ['./recruiter-signup.component.css']
})
export class RecruiterSignupComponent implements OnInit {

  signupObj: any = {
    fullname: '',
    username: '',
    password: '',
    phone: '',
    email: '',
    company: '',
    city: ''
  };
  result: any = {};
  signUpFail = '';
  showPassword = false;
  passwordType = 'password'

  @ViewChild('password') password: ElementRef<HTMLInputElement>;

  constructor(private _recruiterService: RecruiterService,
    private _router: Router,
    private _title: Title) {


  }
  onInputChange(e){
    if(e.target.value != ''){
      this.signUpFail = ''
    }
  }
  clickCheckbox(id) {
    document.getElementById(id).click()
  }

  onSignUp(form: NgForm) {
    console.log(this.signupObj)
    if (this.signupObj) {
      this._recruiterService.onSignUpRecruiter(this.signupObj).subscribe(data => {
        this.result = data;
        console.log(data);
        if (this.result.success == true) {
          // console.log(data);
          //this._toastr.success('Log in successful.', 'Success');

          localStorage.setItem('recruiter', this.result.recruiter);
          localStorage.setItem('uid', this.result.uid);
          localStorage.setItem('token', this.result.token);
          this._router.navigateByUrl("/recruiter/dashboard");
        }
      }, (err) => {
        if (err.status == 400) {
          // this._toastr.error(err.error.msg);
          this.signUpFail = 'Invalid username or password';
        }
      })
    }
  }
  onLogin(){
    this._router.navigate(['/recruiter/login'])
  }
  onCancel() {
    this._router.navigate(['/']);
  }

  ngOnInit(): void {
    this._title.setTitle('Recruiter - Sign Up');
  }

  toggleShow() {
    this.showPassword = !this.showPassword
    if (!this.showPassword) {
      this.passwordType = 'password'
    }
    else {
      this.passwordType = 'text'
    }
  }
}
