import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterNewpasswordComponent } from './recruiter-newpassword.component';

describe('RecruiterNewpasswordComponent', () => {
  let component: RecruiterNewpasswordComponent;
  let fixture: ComponentFixture<RecruiterNewpasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterNewpasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterNewpasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
