import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormsModule, NgModel } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { RecruiterService } from "../../shared/recruiter/recruiter.service";
@Component({
  selector: 'app-recruiter-newpassword',
  templateUrl: './recruiter-newpassword.component.html',
  styleUrls: ['./recruiter-newpassword.component.css']
})
export class RecruiterNewpasswordComponent implements OnInit {

  isMailIncorrect = false
  resetObj = {
    newPassVal: '',
    confNewPassVal: ''
  }
  userId: any
  userToken: any

  result: any = {};
  confNewPassFail = '';
  showErr = '';
  constructor(
    private _router: Router,
    private _title: Title,
    private routeParam: ActivatedRoute,
    private recruiterService: RecruiterService
  ) {

  }

  ngOnInit(): void {
    this._title.setTitle('Recruiter - Forgot Password');
    this.userId = this.routeParam.snapshot.paramMap.get('id');
    this.userToken = this.routeParam.snapshot.paramMap.get('token');

  }
  onInputChange(e) {
    if (e.target.value != '') {
      this.confNewPassFail = ''
    }
  }


  onNewPass(form: NgForm) {
    console.log(this.resetObj)
    if (this.resetObj.newPassVal != this.resetObj.confNewPassVal) {
      this.confNewPassFail = "Password and Confirm Password doesn't match."
    }
    else {
      let updatedPass = {
        password: this.resetObj.newPassVal
      }
      this.recruiterService.onVerifyEmail(this.userId, this.userToken, updatedPass).subscribe(data => {
        console.log(data)
        if (data.sucess) {
          this._router.navigate(['/recruiter/login']);
        }
      }, (err) =>{
        if(err.status == 400){
          this.showErr = err.error?.message
        }
      })
    }
    // this._router.navigate(['/recruiter/newpassword']);

  }


}
