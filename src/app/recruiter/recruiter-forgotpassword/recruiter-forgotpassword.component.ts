import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormsModule, NgModel } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RecruiterService } from 'src/app/shared/recruiter/recruiter.service';
@Component({
  selector: 'app-recruiter-forgotpassword',
  templateUrl: './recruiter-forgotpassword.component.html',
  styleUrls: ['./recruiter-forgotpassword.component.css']
})
export class RecruiterForgotpasswordComponent implements OnInit {

  isMailIncorrect = false
  emailVal = ''
  result: any = {};
  resetFail = '';
  resetSuccess = ''
  constructor(
    private _router: Router,
    private _title: Title,
    private _recruiter: RecruiterService) {


  }

  ngOnInit(): void {
    this._title.setTitle('Recruiter - Forgot Password');

  }
  onInputChange(e) {
    if (e.target.value != '') {
      this.resetFail = ''
    }
  }
  login() {
    this._router.navigate(['/recruiter/login']);
  }


  onReset(form: NgForm) {
    console.log(this.emailVal)
    let email ={
      email:this.emailVal
    }
    this._recruiter.onForgotPassword(email).subscribe(res => {
      if(res.sucess){
        this.resetSuccess = res.message
      }
      // else{
      //   alert(res.message)
      // }
      // this._router.navigate(['/recruiter/newpassword']);

    }, (err) =>{
      if(err.status == 400){
        this.resetFail = err.error?.message
        console.log(err.error?.message)
      }
    })

  }



}
