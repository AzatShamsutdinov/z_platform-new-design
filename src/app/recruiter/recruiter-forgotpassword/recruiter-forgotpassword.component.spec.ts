import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecruiterForgotpasswordComponent } from './recruiter-forgotpassword.component';

describe('RecruiterForgotpasswordComponent', () => {
  let component: RecruiterForgotpasswordComponent;
  let fixture: ComponentFixture<RecruiterForgotpasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RecruiterForgotpasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruiterForgotpasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
