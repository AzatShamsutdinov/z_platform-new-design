import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recruiter-dashboard',
  templateUrl: './recruiter-dashboard.component.html',
  styleUrls: ['./recruiter-dashboard.component.css']
})
export class RecruiterDashboardComponent implements OnInit {

  constructor(private _title: Title,private _router: Router) { }

  ngOnInit(): void {
    this._title.setTitle('Dashboard');
  }
  onAdd() {
    this._router.navigate(['/recruiter/add-vacancy']);
  }

}
