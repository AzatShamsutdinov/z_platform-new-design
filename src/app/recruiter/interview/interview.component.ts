import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { CandidateService } from 'src/app/shared/recruiter/candidate.service';
import { RecruiterService } from 'src/app/shared/recruiter/recruiter.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.css']
})
export class InterviewComponent implements OnInit {

  p: number = 1;
  vacancyId: any;
  vacancyIdObj: any = {
    id: ''
  };
  interview: '';
  name = '';
  btnMute = '';
  score = [];
  skillsArray = [];
  videoDuration = null;
  base_url: any = environment.BASE_URL;
  commentsArray = []
  finalComments = []
  recruiterName= ''
  commentslist = []

  constructor(private _title: Title,
    private route: ActivatedRoute,
    private _interview: CandidateService,
    private _recruiter: RecruiterService) { }

  @ViewChild('videoElement') videoElement: ElementRef;
  @ViewChild('btnPlay') btnPlay: ElementRef;
  @ViewChild('progressBar') progressBar: ElementRef

  onGetInterview(id) {
    this.vacancyIdObj.id = id;
    this._interview.onShowInterview(this.vacancyIdObj).subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.interview = result.data;
        this.name = result.data[0].NAME

        result.scores?.forEach(element => {
          let commentsObject = {
            score_id: 0,
            area_id: 0,
            skills_name: ''
          }
          if (element.count >= 13 && element.count <= 18) {
            commentsObject.score_id = 3
            commentsObject.area_id = element.area_id
            commentsObject.skills_name = element.skills_name
          } else if (element.count >= 19 && element.count <= 25) {
            commentsObject.score_id = 4
            commentsObject.area_id = element.area_id
            commentsObject.skills_name = element.skills_name

          } else if (element.count >= 26 && element.count <= 30) {
            commentsObject.score_id = 5
            commentsObject.area_id = element.area_id
            commentsObject.skills_name = element.skills_name

          }
          if (commentsObject.score_id != 0) {
            this.commentsArray.push(commentsObject)
          }
        });
        console.log('commentsArray: ', this.commentsArray)
        let maxVal = {
          area_id: null,
          score_id: null,
          skills_name: ''
        }
        if (this.commentsArray.length > 0) {
          this.commentsArray.forEach((item, i) => {

            if (item.skills_name == this.commentsArray[i + 1]?.skills_name) {
              if (item.score_id < this.commentsArray[i + 1].score_id) {
                this.commentsArray.splice(i, 1)
              }
              else if (item.score_id > this.commentsArray[i + 1].score_id) {
                this.commentsArray.splice((i + 1), 1)
              }
              else if (item.score_id = this.commentsArray[i + 1].score_id) {
                this.commentsArray.splice(i, 1)
              }
            }

            if (i == 0) {
              if (item.score_id > this.commentsArray[i + 1]?.score_id) {
                maxVal.score_id = item.score_id
                maxVal.skills_name = item.skills_name
                maxVal.area_id = item.area_id
              }
              else if (item.score_id < this.commentsArray[i + 1]?.score_id) {
                maxVal.score_id = this.commentsArray[i + 1].score_id
                maxVal.skills_name = this.commentsArray[i + 1].skills_name
                maxVal.area_id = this.commentsArray[i + 1].area_id
              }
            }
            else{
              if (item.score_id > maxVal.score_id) {
                maxVal.score_id = item.score_id
                maxVal.skills_name = item.skills_name
                maxVal.area_id = item.area_id
              }
              else if (item.score_id < maxVal.score_id) {
                maxVal.score_id = maxVal.score_id
                maxVal.skills_name = maxVal.skills_name
                maxVal.area_id = maxVal.area_id
              }
            }

            // if (this.commentsArray.length > 1) {
            //   let commentsObj = []
            //   let scoreIds = []
            //   commentsObj = this.commentsArray
            //   let val = Math.max.apply(Math, commentsObj.map(function (o) { return o.score_id; }))
            //   console.log('i: ', val)

            //   commentsObj.forEach((item,i) => {
            //     if(item.score_id == val){
            //       commentsObj.splice(i,1)
            //     }
            //   })

            //   let val2 = Math.max.apply(Math, commentsObj.map(function (o) {  return o.score_id; }))



            // }



          })
        }
        console.log('commentsArray: ', this.commentsArray)
        console.log('maxval : ', maxVal)

        this.finalComments.push(maxVal)

        this.commentsArray.forEach((item,i)=> {
          if(item.score_id == maxVal.score_id && item.area_id == maxVal.area_id){
            this.commentsArray.splice(i,1)
          }
        })
        console.log('new commentsArray: ', this.commentsArray)

        if(this.commentsArray.length == 1){
          this.finalComments.push(this.commentsArray[0])
        }
        else if(this.commentsArray.length>1){
          this.commentsArray.forEach((item,i) => {
            if (i == 0) {
              if (item.score_id > this.commentsArray[i + 1]?.score_id) {
                maxVal.score_id = item.score_id
                maxVal.skills_name = item.skills_name
                maxVal.area_id = item.area_id
              }
              else if (item.score_id < this.commentsArray[i + 1]?.score_id) {
                maxVal.score_id = this.commentsArray[i + 1].score_id
                maxVal.skills_name = this.commentsArray[i + 1].skills_name
                maxVal.area_id = this.commentsArray[i + 1].area_id
              }
            }
            else{
              if (item.score_id > maxVal.score_id) {
                maxVal.score_id = item.score_id
                maxVal.skills_name = item.skills_name
                maxVal.area_id = item.area_id
              }
              else if (item.score_id < maxVal.score_id) {
                maxVal.score_id = maxVal.score_id
                maxVal.skills_name = maxVal.skills_name
                maxVal.area_id = maxVal.area_id
              }
            }
          })
          this.finalComments.push(maxVal)
        }
        console.log('new final: ', this.finalComments)

        this.finalComments.forEach(item => {
          let newCommentObject = {
            areaId : item.area_id,
            scoreId : item.score_id
          }
  
          this._interview.onGetComments(newCommentObject).subscribe(data => {
            console.log('res: ', data);
            if(data.success){
              if(data.comments.length > 0){
                this.commentslist.push(data.comments[0]?.name)
              }
            }
          })
        })

       
        this.score = result.scores;

        this.score.forEach((item, i) => {

          if (item.skills_name == this.score[i + 1]?.skills_name) {
            if (item.count != 0 && this.score[i + 1] != 0) {
              item.count = Math.round((item.count + this.score[i + 1].count) / 2)
            }
            else {
              item.count = item.count + this.score[i + 1].count
            }
            this.score.splice((i + 1), 1)
          }
        })

        console.log('scores: ', this.score)


      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })

    setTimeout(() => {
      this.getSkills();


    }, 2000);

  }

  ngOnInit(): void {

    this._title.setTitle('View - Interview');
    this.vacancyId = this.route.snapshot.paramMap.get('id');
    this.onGetInterview(this.vacancyId);

    this._recruiter.getUsers().subscribe(data => {
      if (data.success) {
        if (data.data.length > 0) {
          this.recruiterName = data.data[0].FULLNAME;
         
        }
      }
    })
    
    // player.addEventListener('volumechange', function (e) {
    //   // Update the button to be mute/unmute
    //   if (player.muted) changeButtonType(btnMute, 'unmute');
    //   else changeButtonType(btnMute, 'mute');
    // }, false);

    // let player = document.getElementById('video-element') as any;
    // let btnplay = document.getElementById('btnplay') as any;
    this.btnMute = document.getElementById('btnMute') as any;
    // let progressBar = document.getElementById('progress-bar') as any;
    // // let volumeBar = document.getElementById('volume-bar') as any;


  }

 
  updateTime() {
    this.updateProgressBar()
  }

  ngOnChanges(): void {
    // this.videoElement.nativeElement.ontimeupdate(() => this.updateProgressBar(), false);
    this.videoElement.nativeElement.play(this.changeButtonType(this.btnPlay, 'pause'), false)
    this.videoElement.nativeElement.pause(this.changeButtonType(this.btnPlay, 'play'), false)
    this.videoElement.nativeElement.ended(this.videoElement.nativeElement.pause(), false)


    this.videoElement.nativeElement.volumechange((e) => {
      // Update the button to be mute/unmute
      if (this.videoElement.nativeElement.muted) { this.changeButtonType(this.btnMute, 'unmute') }
      else {
        this.changeButtonType(this.btnMute, 'mute');
      }
    }, false);

  }

  loadmetadata(e) {
    console.log('event', e)
    this.videoDuration = e.target.duration
  }

  openFullscreen() {
    if (this.videoElement.nativeElement.requestFullscreen) {
      this.videoElement.nativeElement.requestFullscreen();
    } else if (this.videoElement.nativeElement.webkitRequestFullscreen) { /* Safari */
      this.videoElement.nativeElement.webkitRequestFullscreen();
    } else if (this.videoElement.nativeElement.msRequestFullscreen) { /* IE11 */
      this.videoElement.nativeElement.msRequestFullscreen();
    }
  }

  updateProgressBar() {

    var percentage = Math.floor((100 / this.videoElement.nativeElement.duration) * this.videoElement.nativeElement.currentTime);
    // Update the progress bar's value

    console.log('percentage up: ', this.videoElement.nativeElement.duration)
    this.progressBar.nativeElement.value = percentage;
    console.log('percentage: ', percentage)
    // Update the progress bar's text (for browsers that don't support the progress element)
    this.progressBar.nativeElement.innerHTML = percentage + '% played';
  }

  volumeBar(e) {
    console.log('volume:', this.videoElement)
    this.videoElement = e.target.value;
  };

  changeButtonType(btn, value) {
    console.log('btn:', typeof (value))
    btn.attributes.title.value = value;
    btn.innerHTML = value;
    btn.attributes.class.nodeValue = value;
  }

  playVideo() {
    if (this.videoElement.nativeElement.paused || this.videoElement.nativeElement.ended) {
      // Change the button to a pause button 
      this.changeButtonType(this.btnPlay.nativeElement, 'pause');
      this.videoElement.nativeElement.play();
    } else {
      // Change the button to a play button
      this.changeButtonType(this.btnPlay.nativeElement, 'play');
      this.videoElement.nativeElement.pause();
      // this.progressBar.nativeElement.click((e) => this.seek(e));

    }
    console.log(" vide duration : ", this.videoDuration)
  }

  getSkills() {
    this._interview.onGetSkills().subscribe(item => {
      item.data.forEach(element => {
        let scoringArray = {
          skill_name: '',
          scores: [
            {
              score_name: 'Base',
              active: false,
              definition: null
            },
            {
              score_name: 'Developing',
              active: false,
              definition: null
            },
            {
              score_name: 'Intermediate',
              active: false,
              definition: null
            },
            {
              score_name: 'Advanced',
              active: false,
              definition: null
            },
            {
              score_name: 'Expert',
              active: false,
              definition: null
            }
          ],
          tooltip: ''
        }
        scoringArray.skill_name = element.name;
        scoringArray.tooltip = element.definition

        this.score.forEach(item => {
          if (item.skills_name == element.name) {
            if (item.count < 7) {
              scoringArray.scores[0].active = true
              if (scoringArray.skill_name == 'Communication') {
                scoringArray.scores[0].definition = 'Not very likely to manifest the ability to actively listen and comprehend information in a work setting.'
              } else if (scoringArray.skill_name == 'Adaptability') {
                scoringArray.scores[0].definition = 'Not very likely to manifest the ability to adjust behaviour in order to deal effectively with changes in the workplace.'
              } else if (scoringArray.skill_name == 'Leadership') {
                scoringArray.scores[0].definition = 'Not very likely to manifest the ability to delegate authority and task responsibility in a work setting.'
              } else if (scoringArray.skill_name == 'Problem Solving') {
                scoringArray.scores[0].definition = 'Not very likely to the ability to analyse, identify and define a problem in a work setting.'
              } else if (scoringArray.skill_name == 'Teamwork') {
                scoringArray.scores[0].definition = 'Not very likely to manifest the ability to support colleagues by listening and encouraging participation in a work setting.'
              } else if (scoringArray.skill_name == 'Time Management') {
                scoringArray.scores[0].definition = 'Not very likely to manifest the ability to prioritise tasks in a work setting.'
              }
            }
            else if (item.count >= 7 && item.count < 13) {
              scoringArray.scores[1].active = true
              if (scoringArray.skill_name == 'Communication') {
                scoringArray.scores[1].definition = 'Likely to manifest the ability to actively listen and comprehend information in a work setting from time to time, but to a lower degree.'
              } else if (scoringArray.skill_name == 'Adaptability') {
                scoringArray.scores[1].definition = 'Likely to manifest the ability to adjust behaviour in order to deal effectively with changes in the workplace from time to time, but to a lower degree.'
              } else if (scoringArray.skill_name == 'Leadership') {
                scoringArray.scores[1].definition = 'Likely to manifest the ability to delegate authority and task responsibility in a work setting from time to time, but to a lower degree.'
              } else if (scoringArray.skill_name == 'Problem Solving') {
                scoringArray.scores[1].definition = 'Likely to the ability to analyse, identify and define a problem in a work setting from time to time, but to a lower degree.'
              } else if (scoringArray.skill_name == 'Teamwork') {
                scoringArray.scores[1].definition = 'Likely to manifest the ability to support colleagues by listening and encouraging participation in a work setting from time to time, but to a lower degree.'
              } else if (scoringArray.skill_name == 'Time Management') {
                scoringArray.scores[1].definition = 'Likely to manifest the ability to prioritise tasks in a work setting from time to time, but to a lower degree.'
              }
            }
            else if (item.count >= 13 && item.count < 19) {
              scoringArray.scores[2].active = true
              if (scoringArray.skill_name == 'Communication') {
                scoringArray.scores[2].definition = 'Likely to manifest the ability to actively listen and comprehend information in a work setting sometimes, but to a higher degree.'
              } else if (scoringArray.skill_name == 'Adaptability') {
                scoringArray.scores[2].definition = 'Likely to manifest the ability to adjust behaviour in order to deal effectively with changes in the workplace sometimes, but to a higher degree.'
              } else if (scoringArray.skill_name == 'Leadership') {
                scoringArray.scores[2].definition = 'Likely to manifest the ability to delegate authority and task responsibility in a work setting sometimes, but to a higher degree.'
              } else if (scoringArray.skill_name == 'Problem Solving') {
                scoringArray.scores[2].definition = 'Likely to manifest the ability to analyse, identify and define a problem in a work setting sometimes, but to a higher degree.'
              } else if (scoringArray.skill_name == 'Teamwork') {
                scoringArray.scores[2].definition = 'Likely to manifest the ability to support colleagues by listening and encouraging participation in a work setting sometimes, but to a higher degree.'
              } else if (scoringArray.skill_name == 'Time Management') {
                scoringArray.scores[2].definition = 'Likely to manifest the ability to prioritise tasks in a work setting sometimes, but to a higher degree.'
              }
            }
            else if (item.count >= 19 && item.count < 26) {
              scoringArray.scores[3].active = true
              if (scoringArray.skill_name == 'Communication') {
                scoringArray.scores[3].definition = 'Likely to manifest the ability to actively listen and comprehend information in a work setting most of the time.'
              } else if (scoringArray.skill_name == 'Adaptability') {
                scoringArray.scores[3].definition = 'Likely to manifest the ability to adjust behaviour in order to deal effectively with changes in the workplace most of the time.'
              } else if (scoringArray.skill_name == 'Leadership') {
                scoringArray.scores[3].definition = 'Likely to manifest the ability to delegate authority and task responsibility in a work setting most of the time.'
              } else if (scoringArray.skill_name == 'Problem Solving') {
                scoringArray.scores[3].definition = 'Likely to manifest the ability to analyse, identify and define a problem in a work setting most of the time.'
              } else if (scoringArray.skill_name == 'Teamwork') {
                scoringArray.scores[3].definition = 'Likely to manifest the ability to support colleagues by listening and encouraging participation in a work setting most of the time.'
              } else if (scoringArray.skill_name == 'Time Management') {
                scoringArray.scores[3].definition = 'Likely to manifest the ability to prioritise tasks in a work setting most of the time.'
              }
            }
            else if (item.count >= 26 && item.count <= 30) {
              scoringArray.scores[4].active = true
              if (scoringArray.skill_name == 'Communication') {
                scoringArray.scores[4].definition = 'Likely to always manifest the ability to actively listen and comprehend information in a work setting.'
              } else if (scoringArray.skill_name == 'Adaptability') {
                scoringArray.scores[4].definition = 'Likely to always manifest the ability to adjust behaviour in order to deal effectively with changes in the workplace.'
              } else if (scoringArray.skill_name == 'Leadership') {
                scoringArray.scores[4].definition = 'Likely to always manifest the ability to delegate authority and task responsibility in a work setting.'
              } else if (scoringArray.skill_name == 'Problem Solving') {
                scoringArray.scores[4].definition = 'Likely to always manifest the ability to analyse, identify and define a problem in a work setting.'
              } else if (scoringArray.skill_name == 'Teamwork') {
                scoringArray.scores[4].definition = 'Likely to always manifest the ability to support colleagues by listening and encouraging participation in a work setting.'
              } else if (scoringArray.skill_name == 'Time Management') {
                scoringArray.scores[4].definition = 'Likely to always manifest the ability to prioritise tasks in a work setting.'
              }
            }
          }
        })


        this.skillsArray.push(scoringArray)
      });
      console.log(this.skillsArray)
    })
  }
}



