import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common'
import { RecruiterService } from "../../../shared/recruiter/recruiter.service";

@Component({
  selector: 'app-recruiter-header',
  templateUrl: './recruiter-header.component.html',
  styleUrls: ['./recruiter-header.component.css']
})
export class RecruiterHeaderComponent implements OnInit {

  recruiter = '';
  recruiterName = '';
  recruiterEmail = ''
  @Input() showTitle: string;
  @Input() candidateName: string
  @Input() vacancyId: string
  
  @Output() sendUsersName = new EventEmitter<any>();

  constructor(private _router: Router, private location: Location, private _recruiter: RecruiterService) { }


  onRecruiterLogout() {
    if (confirm('Are you sure you want to logged out ?')) {
      localStorage.clear();
      this._router.navigate(['/recruiter/login']);
    }
  }

  ngOnInit(): void {
    // console.log('title: ', this.candidateName)
    this.recruiter = localStorage.getItem('recruiter');
    this._recruiter.getUsers().subscribe(data => {
      if (data.success) {
        if (data.data.length > 0) {
          this.recruiterName = data.data[0].FULLNAME;
          this.recruiterEmail = data.data[0].EMAIL
          this.sendUsersName.emit(this.recruiterName)
        }
      }
    })
  }
  onAdd() {
    this._router.navigate(['/recruiter/add-vacancy']);
  }
  back() {
    this.location.back()
  }
}
