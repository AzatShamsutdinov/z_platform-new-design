import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { CandidateService } from 'src/app/shared/recruiter/candidate.service';

@Component({
  selector: 'app-edit-candidate',
  templateUrl: './edit-candidate.component.html',
  styleUrls: ['./edit-candidate.component.css']
})
export class EditCandidateComponent implements OnInit {

  candidateId: any;
  candidates: any = {};
  // cities: any = {};
  candidateObj: any = {
    name: '',
    job_title: '',
    company: '',
    email: '',
    phone: '',
    city: '',
    id: ''
  };
  candidateIdObj: any = {
    id: ''
  };
  updateMsg = '';

  constructor(private _router: Router,
              private _route: ActivatedRoute,
              private _candidateService: CandidateService,
              private _title: Title) { }

  onGetCandidate(id) {
    this.candidateIdObj.id=id;
    this._candidateService.onGetCandidate(this.candidateIdObj).subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.candidateObj.name = result.data[0].NAME;
        this.candidateObj.job_title = result.data[0].JOB_TITLE;
        this.candidateObj.company = result.data[0].COMPANY;
        this.candidateObj.email = result.data[0].EMAIL;
        this.candidateObj.phone = result.data[0].PHONE;
        this.candidateObj.city = result.data[0].CITY;
        this.candidateObj.id = result.data[0].C_ID;
        console.log(result.data);
      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })
  }

  // onGetCity() {
  //   this._candidateService.onGetCities().subscribe(data => {
  //     let result: any = data;
  //     if (result.success == true) {
  //       this.cities = result.data;
  //       console.log(this.cities);
  //     }
  //   }, (err) => {
  //     if (err.status == 400) {
  //       console.log(err.error.msg);
  //     }
  //   })
  // }

  onUpdateCandidate(form: NgForm) {    
    this._candidateService.onUpdateCandidate(this.candidateObj).subscribe(data => {
      let result: any = data;
      if (result.success) {
        this.updateMsg = 'Candidate updated successfully.'
      }
    }, err => {
      console.log(err.msg);
    })
  }

  onCancel() {
    this._router.navigate(['/recruiter/candidate']);
  }

  ngOnInit(): void {
    this._title.setTitle('Candidate - Edit')
    this.candidateId=this._route.snapshot.paramMap.get('id');
    this.onGetCandidate(this.candidateId);
    // this.onGetCity();
  }

}
