import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { CandidateService } from 'src/app/shared/recruiter/candidate.service';

@Component({
  selector: 'app-add-candidate',
  templateUrl: './add-candidate.component.html',
  styleUrls: ['./add-candidate.component.css']
})
export class AddCandidateComponent implements OnInit {

  candidateObj: any = {
    name: '',
    job_title: '',
    company: '',
    email: '',
    phone: '',
    city_id: ''
  };
  cities: any = {};
  successMsg = '';

  constructor(private _router: Router,
              private _candidateService: CandidateService,
              private _title: Title) { }

  onCancel() {
    this._router.navigate(['/recruiter/candidate']);
  }

  onGetCity() {
    this._candidateService.onGetCities().subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.cities = result.data;
        console.log(this.cities);
      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })
  }

  onAddCandidate(form: NgForm) {
    console.log(form)
    console.log(this.candidateObj)
    this._candidateService.onAddCandidate(this.candidateObj).subscribe(data => {
      if (data.success == true) {
        // this._toastr.success('Successfully registered. Please login to continue.', 'Success', {
        //   timeOut: 2000,
        //   positionClass: 'toast-center-center',
        //   progressBar: true,
        //   progressAnimation: 'increasing'
        // });
        this.successMsg = 'Candidate added successfully.';
        form.resetForm();
      }
    }, e => {
      if (e.status == 400) {
        // this._toastr.error(e.error.msg, 'Error', {
        //   timeOut: 2000,
        //   positionClass: 'toast-center-center',
        //   progressBar: true,
        //   progressAnimation: 'increasing'
        // });
        console.log('Error');
      }
    })
  }

  ngOnInit(): void {
    this._title.setTitle('Candidate - Add')
    this.onGetCity();
  }

}
