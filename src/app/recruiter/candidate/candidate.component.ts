import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { CandidateService } from 'src/app/shared/recruiter/candidate.service';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.css']
})


export class CandidateComponent implements OnInit {

  searchedKeyword : string
  result: any = {};
  deleteSucc = '';
  candidates: any;
  candidatesSearchArr: any;
  isCopy = false;
  showToastMsg = false;

  constructor(private _candidateService: CandidateService,
              private _router: Router,
              private _title: Title) { }

  onAdd() {
    this._router.navigate(['/recruiter/add-candidate']);
  }

  onEdit(id) {
    this._router.navigate(['/recruiter/edit-candidate', id]);
  }

  onGetCandidates() {
    this._candidateService.onGetCandidates().subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.candidates = result.data;
        this.candidatesSearchArr = this.candidates
        console.log(this.candidates);
      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })
  }

  onSearch(e) {
    let searchKey = e.target.value;
    if (searchKey) {
      this.candidatesSearchArr = this.candidates.filter((data) => {
          let arrObj: any = data.COMPANY + "," + data.NAME +  "," + data.JOB_TITLE;
          let d = JSON.stringify(arrObj);
          if (d.toLowerCase().includes(searchKey.toLowerCase())) return data;
        });

    } else {
      this.candidatesSearchArr = this.candidates
  }
}

onCopy(i) {
  this.candidatesSearchArr[i].isCopy = true;

  this.showToastMsg = true;
    setTimeout(() => {
      this.showToastMsg = false
    }, 3000);
}
  onDeleteCandidate(id) {
    if(confirm('Are you sure you want to delete this candidate ?')) {
      this._candidateService.onDeleteCandidate(id).subscribe(data => {
        let result: any = data;
        if (result.success) {
          this.onGetCandidates();
          this.deleteSucc = 'Candidate deleted successfully.'
        }
      }, err => {
        console.log(err.msg);
      })
    }    
  }  

  onViewInterview(id) {
    this._router.navigate(['/recruiter/view-interview', id]);
  }

  ngOnInit(): void {
    this._title.setTitle('Candidates')
    this.onGetCandidates();
  }

}
