import { Component, ElementRef, OnInit, ViewChild, OnChanges, Output, EventEmitter, Input, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { QuestionService } from 'src/app/shared/recruiter/question.service';
import { VacancyService } from 'src/app/shared/recruiter/vacancy.service';

@Component({
  selector: 'app-add-question',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent {

  // defaultques = ['ques1', 'ques2', 'ques3', 'ques4', 'ques5', 'ques6', 'ques7', 'ques8', 'ques9'];
  defaultques = [];
  questions: any = [];
  hiddenQuestions: any = [];
  result: any = {};
  deleteSucc = '';
  questionId = 0;

  questionObj: any = {
    // vacancy_id: '',
    question: ''
  };
  showErrMsg = ''

  successMsg = '';
  showAddQuestionIpt = false;
  hideQuestionMark = false
  vacancyId: any;
  vacancyIdObj: any = {
    vacancy_id: '',
  };
  clearQueList = false;
  constructor(private _title: Title,
    private _questionService: QuestionService,
    private route: ActivatedRoute) {

  }

  @Input() getSkillQuesArr: [];
  @ViewChild('delQuestionModal') delQuestionModal: ElementRef;
  @Output() sendQuestions = new EventEmitter<any>();
  @Input() isClear = false;

  @Input()
  selectedSkillsArray: []
 
  onGetQuestion(id) {
    this.vacancyIdObj.vacancy_id = id;
    this._questionService.onGetQuestion(this.vacancyIdObj).subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.questions = result.data;
      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })
  }

  onAddQuestion() {
    // this.questionObj.vacancy_id = this.vacancyId;
    this.questions.push(this.questionObj.question)
    this.questionObj.question = ''
    this.sendQuestions.emit(this.questions);

    // console.log(this.questions)

    // this._questionService.onAddQuestion(this.questionObj).subscribe(data => {
    //   if (data.success == true) {
    //     // this._toastr.success('Successfully registered. Please login to continue.', 'Success', {
    //     //   timeOut: 2000,
    //     //   positionClass: 'toast-center-center',
    //     //   progressBar: true,
    //     //   progressAnimation: 'increasing'
    //     // });
    //     this.successMsg = 'Question added successfully.';
    //     this.questionObj.question = ''
    //     this.ngOnInit();
    //   }
    // }, e => {
    //   if (e.status == 400) {
    //     // this._toastr.error(e.error.msg, 'Error', {
    //     //   timeOut: 2000,
    //     //   positionClass: 'toast-center-center',
    //     //   progressBar: true,
    //     //   progressAnimation: 'increasing'
    //     // });
    //     console.log('Error');
    //   }
    // })
  }

  onDeleteQuestion(id) {
    // this._questionService.onDeleteQuestion(id).subscribe(data => {
    //   let result: any = data;
    //   if (result.success) {
    //     this.delQuestionModal.nativeElement.attributes[2].value = "modal fade"
    //     this.delQuestionModal.nativeElement.ariaHidden = 'true'
    //     this.delQuestionModal.nativeElement.ariaModal = null
    //     this.delQuestionModal.nativeElement.style.display = 'none'
    //     let backdrop = document.getElementsByClassName('modal-backdrop')
    //     backdrop[0].parentNode.removeChild(backdrop[0])
    //     document.getElementsByTagName('body')[0].classList.remove('modal-open')


    //     this.onGetQuestion(this.vacancyId);
    //     this.deleteSucc = 'Question deleted successfully.'
    //   }
    // }, err => {
    //   console.log(err.msg);
    // })
    this.questions.splice(id, 1)
    this.delQuestionModal.nativeElement.attributes[2].value = "modal fade"
    this.delQuestionModal.nativeElement.ariaHidden = 'true'
    this.delQuestionModal.nativeElement.ariaModal = null
    this.delQuestionModal.nativeElement.style.display = 'none'
    let backdrop = document.getElementsByClassName('modal-backdrop')
    backdrop[0].parentNode.removeChild(backdrop[0])
    document.getElementsByTagName('body')[0].classList.remove('modal-open')

    if (this.questions.length == 0) {
      if (!this.showAddQuestionIpt) {
        this.hideQuestionMark = false
      }
    }
  }

  checkIfValid() {
    if (this.questionObj.question == '') {
      this.showErrMsg = "The question field is required!"
    }
    else {
      this.showErrMsg = ''
    }
  }

  // getQuestionsFromId(id) {
  //   this._vacancyService.getQuestionFromId(id).subscribe(data => {
  //     let result = data
  //     if (result.success == true) {
  //       console.log('res: ', result)
  //       this.quesTextArr = result.question
  //       this.hiddenQuestions = this.quesTextArr
  //       console.log('ques Arr : ', this.quesTextArr)
  //       console.log('hiddenques: ' + this.hiddenQuestions + " len: ", this.hiddenQuestions.length)
  //     }
  //   })
  // }
  ngOnInit(): void {
    this._title.setTitle('Vacancy - Question');
  }
  ngOnChanges(changes: SimpleChanges) {


    if (this.isClear) {
      this.questions = []
    }
    
    if (changes.getSkillQuesArr?.currentValue) {
      this.hiddenQuestions = changes.getSkillQuesArr.currentValue
      this.defaultques = changes.getSkillQuesArr.currentValue
    }

    if (changes.selectedSkillsArray?.currentValue) {
      if (changes.selectedSkillsArray.currentValue.length == 6) {
        this.hiddenQuestions = this.defaultques.slice(0, 9)
        
      }
      if (changes.selectedSkillsArray.currentValue.length == 5) {
        this.hiddenQuestions = this.defaultques.slice(0, 8)
      }
      if (changes.selectedSkillsArray.currentValue.length == 4) {
        this.hiddenQuestions = this.defaultques.slice(0, 7)
      }
      if (changes.selectedSkillsArray.currentValue.length == 3) {
        this.hiddenQuestions = this.defaultques.slice(0, 6)
      }
    }
  }

}
