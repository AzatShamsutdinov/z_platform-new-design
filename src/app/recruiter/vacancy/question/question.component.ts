import { Component, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { QuestionService } from 'src/app/shared/recruiter/question.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  questions: any[];
  result: any = {};
  deleteSucc = '';
  questionId = 0 ;
  sendNewQues = []
  questionObj: any = {
    vacancy_id: '',
    question: ''
  };
  showErrMsg = ''
  hideQuestionMark = false

  successMsg = '';
  showAddQuestionIpt = false;
  vacancyId: any;
  vacancyIdObj: any = {
    vacancy_id: ''
  };
  hiddenQuestions  = 0;
  defaultques =  ['ques1','ques2','ques3','ques4','ques5','ques6','ques7','ques8','ques9'];


  constructor(private _title: Title,
              private _questionService: QuestionService,
              private route: ActivatedRoute) { }


  @ViewChild('delQuestionModal') delQuestionModal: ElementRef;
  @Input() isClear = false;
  @Input()
  selectedSkillsArray : [];
  @Output() sendQuestions = new EventEmitter<any>();
  
  ngOnChanges(changes: SimpleChanges) {
    if(this.isClear){
      this.questions = []
    }
    if(changes.selectedSkillsArray.currentValue){
      console.log('selected changes: ', changes.selectedSkillsArray)
      if(changes.selectedSkillsArray.currentValue.length == 3){
        this.hiddenQuestions = 6
      }
      if(changes.selectedSkillsArray.currentValue.length == 4){
        this.hiddenQuestions = 7
      }
      if(changes.selectedSkillsArray.currentValue.length == 5){
        this.hiddenQuestions = 8
      }
      if(changes.selectedSkillsArray.currentValue.length == 6){
        this.hiddenQuestions = 9
      }
     
     
      
    }
  }
  
  onGetQuestion(id) {
    this.vacancyIdObj.vacancy_id=id;
    this._questionService.onGetQuestion(this.vacancyIdObj).subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.questions = result.data;
        if(this.questions.length>0){
          this.hideQuestionMark = true
        }
        console.log(this.questions);
      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })
  }

  onAddQuestion() {
    console.log(this.questionObj)
    this.questions.push({QUESTION : this.questionObj.question, Status: 0}) 
    this.sendNewQues.push({QUESTION : this.questionObj.question, Status: 0})
    this.questionObj.question = ''
    this.sendQuestions.emit(this.sendNewQues);
  }
  
  onDeleteQuestion(id) {
      this._questionService.onDeleteQuestion(id).subscribe(data => {
        let result: any = data;
        if (result.success) {
          this.delQuestionModal.nativeElement.attributes[2].value = "modal fade" 
          this.delQuestionModal.nativeElement.ariaHidden = 'true'
          this.delQuestionModal.nativeElement.ariaModal = null
          this.delQuestionModal.nativeElement.style.display = 'none'
          let backdrop = document.getElementsByClassName('modal-backdrop')
          backdrop[0].parentNode.removeChild(backdrop[0])
          document.getElementsByTagName('body')[0].classList.remove('modal-open')
    
          
          this.onGetQuestion(this.vacancyId);
          this.deleteSucc = 'Question deleted successfully.'
        }
      }, err => {
        console.log(err.msg);
      })

      if(this.questions.length == 0){
        if(!this.showAddQuestionIpt){
          this.hideQuestionMark = false
        }
      }
        
  }

  checkIfValid(){
    if(this.questionObj.question == ''){
      this.showErrMsg = "The question field is required!"
    }
    else{
      this.showErrMsg = ''
    }
  }
  ngOnInit(): void {
    this._title.setTitle('Vacancy - Question');
    this.vacancyId=this.route.snapshot.paramMap.get('id');
    this.onGetQuestion(this.vacancyId);
  }

}


  





