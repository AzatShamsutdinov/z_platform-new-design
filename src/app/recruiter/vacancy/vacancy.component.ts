import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { VacancyService } from 'src/app/shared/recruiter/vacancy.service';

@Component({
  selector: 'app-vacancy',
  templateUrl: './vacancy.component.html',
  styleUrls: ['./vacancy.component.css']
})
export class VacancyComponent implements OnInit {
  searchedKeyword: string;
  vacancies: any[];
  result: any = {};
  deleteSucc = '';
  vacancyId = 0;
  showDeleteModal = false
  showToastMsg = false

  constructor(private _vacancyService: VacancyService,
    private _router: Router,
    private _title: Title) { }


  @ViewChild('delModal') delModal: ElementRef;

  onCopy(i) {
    this.vacancies[i].isCopy = true;
    this.showToastMsg = true;
    setTimeout(() => {
      this.showToastMsg = false
    }, 3000);
  }

  onAdd() {
    this._router.navigate(['/recruiter/add-vacancy']);

  }

  onEdit(id) {

    this._router.navigate(['/recruiter/edit-vacancy', id]);
  }

  onQuestion(id) {
    this._router.navigate(['/recruiter/question', id]);
  }

  getVacancyId(id) {
    this.vacancyId = id;
    
  }
  onGetVacancies() {
    //console.log(localStorage.getItem('token'));
    this._vacancyService.onGetVacancies().subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.vacancies = result.data;
        //console.log(this.vacancies);
      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })
  }

  onDeleteVacancy(id) {

    this.delModal.nativeElement.attributes[2].value = "modal fade" 
    this.delModal.nativeElement.ariaHidden = 'true'
    this.delModal.nativeElement.ariaModal = null
    this.delModal.nativeElement.style.display = 'none'
    let backdrop = document.getElementsByClassName('modal-backdrop')
    backdrop[0].parentNode.removeChild(backdrop[0])
    document.getElementsByTagName('body')[0].classList.remove('modal-open')
    
    this._vacancyService.onDeleteVacancy(id).subscribe(data => {
      let result: any = data;
      if (result.success) {
        
        this.onGetVacancies();
        this.deleteSucc = 'Vacancy deleted successfully.'
      }
    }, err => {
      console.log(err.msg);
    })

  }

  ngOnInit(): void {
    this._title.setTitle('Vacancys');
    this.onGetVacancies();
    
  }

}


// $(document).ready(function(){
//   $("#myInput").on("keyup", function() {
//     var value = $(this).val().toLowerCase();
//     $("#myTable tr").filter(function() {
//       $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
//     });
//   });

// });

