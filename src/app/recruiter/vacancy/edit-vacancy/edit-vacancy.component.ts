import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { VacancyService } from 'src/app/shared/recruiter/vacancy.service';
import { QuestionService } from 'src/app/shared/recruiter/question.service';

@Component({
  selector: 'app-edit-vacancy',
  templateUrl: './edit-vacancy.component.html',
  styleUrls: ['./edit-vacancy.component.css']
})
export class EditVacancyComponent implements OnInit {

  vacancyId: any;
  questionArr = [];
  vacancies: any = {};
  // cities: any = {};
  vacancyObj: any = {
    title: '',
    company: '',
    city: '',
    id: ''
  };
  vacancyIdObj: any = {
    id: ''
  };
  updateMsg = '';
  questionArray: any = [];

  skillSetArray = [
    {
      name: 'Problem solving',

      checked: false
    },
    {
      name: 'Adaptability',

      checked: false

    },
    {
      name: 'Time management',

      checked: false

    },
    {
      name: 'Teamwork',

      checked: false

    },
    {
      name: 'Leadership',

      checked: false

    },
    {
      name: 'Communication',
      checked: false

    }
  ]

  sendSkillArray = []



  constructor(private _router: Router,
    private route: ActivatedRoute,
    private _vacancyService: VacancyService,
    private _questionService: QuestionService,
    private _title: Title) { }

  @ViewChild('saveChangesModal') saveChangesModal: ElementRef


  clickCheckbox(id) {
    document.getElementById(id).click()
  }
  getQuestionArray(quesArray: any) {
    this.questionArray = quesArray
    console.log("Questions parent: ", this.questionArray)
  }
  onGetVacancies(id) {
    this.vacancyIdObj.id = id;
    this._vacancyService.onGetVacancy(this.vacancyIdObj).subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.vacancyObj.title = result.data[0].TITLE;
        this.vacancyObj.company = result.data[0].COMPANY;
        this.vacancyObj.city = result.data[0].CITY;
        this.vacancyObj.id = result.data[0].V_ID;
        console.log(result.data);


        if (result.skillsSet && result.skillsSet.length > 0) {
          let skills = result.skillsSet[0].skills
          let skillsResArr = skills.split(',')
          console.log('arr: ', skillsResArr)
          this.sendSkillArray = [...skillsResArr]
          this.skillSetArray.forEach((row, i) => {
            skillsResArr.forEach(resSkill => {
              if (resSkill == row.name) {
                row.checked = true
              }
            })
          })
        }


      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })
  }

  // onGetCity() {
  //   this._vacancyService.onGetCities().subscribe(data => {
  //     let result: any = data;
  //     if (result.success == true) {
  //       this.cities = result.data;
  //       console.log(this.cities);
  //     }
  //   }, (err) => {
  //     if (err.status == 400) {
  //       console.log(err.error.msg);
  //     }
  //   })
  // }

  onUpdateVacancy(form: NgForm) {
    this._vacancyService.onUpdateVacancy(this.vacancyObj).subscribe(data => {
      let result: any = data;
      if (result.success) {
        let sendNewArray = []
        this.questionArray.forEach(element => {
          let ques = {
            question: element.QUESTION,
            status: element.Status
          }
          sendNewArray.push(ques)
        })
        let questionObj = {
          vacancy_id: this.vacancyId,
          question: sendNewArray
        };
        console.log("send ques obj : ", questionObj)
        this._questionService.onAddQuestion(questionObj).subscribe(data => {
          if (data.success == true) {

            // this.successMsg = 'Question added successfully.';
            questionObj.question = []
          }
        }, e => {
          if (e.status == 400) {
            console.log('Error');
          }
        })
        // });


        this.saveChangesModal.nativeElement.attributes[2].value = "modal fade"
        this.saveChangesModal.nativeElement.ariaHidden = 'true'
        this.saveChangesModal.nativeElement.ariaModal = null
        this.saveChangesModal.nativeElement.style.display = 'none'
        let backdrop = document.getElementsByClassName('modal-backdrop')
        backdrop[0].parentNode.removeChild(backdrop[0])
        document.getElementsByTagName('body')[0].classList.remove('modal-open')

        this.updateMsg = 'Vacancy updated successfully.'
        setTimeout(() => {
          this.updateMsg = ''
        }, 5000);
      }
    }, err => {
      console.log(err.msg);
    })
  }

  onCancel() {
    this._router.navigate(['/recruiter/vacancy']);
  }

  ngOnInit(): void {
    this._title.setTitle('Vacancy - Edit');
    this.vacancyId = this.route.snapshot.paramMap.get('id');
    this.onGetVacancies(this.vacancyId);
    // this.onGetCity();
  }

}
