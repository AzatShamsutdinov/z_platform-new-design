import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { VacancyService } from 'src/app/shared/recruiter/vacancy.service';
import { QuestionService } from 'src/app/shared/recruiter/question.service';
import { AddQuestionComponent } from '../add-question/add-question.component';
import { RecruiterService } from 'src/app/shared/recruiter/recruiter.service';

@Component({
  selector: 'app-add-vacancy',
  templateUrl: './add-vacancy.component.html',
  styleUrls: ['./add-vacancy.component.css']
})
export class AddVacancyComponent implements OnInit {

  // cities: any = {};

  vacancyObj: any = {
    title: '',
    company: '',
    city: '',
    skills: []
  };
  firstInIt = true
  // passSkillSet: any = [];
  skillQuesArr: any = [];
  questionArray: any = [];
  skillSetArray = [];
  checkSkillSetArray = [];
  selectedSkillsArray = ['checkbox1', 'checkbox2', 'checkbox3', 'checkbox4', 'checkbox5', 'checkbox6']
  sendSkillsArray = []
  quesIdArr = []
  successMsg = '';
  clearQueList = false;
  getLink = ""
  isCopy = false
  quesIdobj: any = {
    quesId: []
  }
  quesTextArr = []
  componentQuestionArray = []


  constructor(private _router: Router,
    private _vacancyService: VacancyService,
    private _questionService: QuestionService,
    private _recruiterService: RecruiterService,
    private _title: Title) { }



  onCancel() {
    this._router.navigate(['/recruiter/vacancy']);
  }

  clickCheckbox(id) {

    if (!(document.getElementById(id) as any).disabled) {
      document.getElementById(id).click()
      if ((document.getElementById(id) as any).checked) {
        this.selectedSkillsArray.push(id);
      }
      else {
        const index = this.selectedSkillsArray.indexOf(id)
        this.selectedSkillsArray.splice(index, 1)
      }
    }


    if (this.selectedSkillsArray.length == 3) {
      this.selectedSkillsArray.forEach(disablerow => {
        (document.getElementById(disablerow) as any).disabled = true;
        let index = parseInt(disablerow.split('checkbox')[1]) - 1;

        this.skillSetArray[index].tooltip = 'You need to select at least 3 skills  for the vacancy.'
        // (document.getElementById(disablerow) as any).setAttribute('data-tooltip', 'You need to select at least 3 skills  for the vacancy.');
      })
    }
    else {
      this.selectedSkillsArray.forEach(disablerow => {
        (document.getElementById(disablerow) as any).disabled = false;
        let index = parseInt(disablerow.split('checkbox')[1]) - 1;
        // let i = parseInt(index)
        let tooltipval = this.checkSkillSetArray[index].tooltip
        this.skillSetArray[index].tooltip = tooltipval

        // (document.getElementById(disablerow) as any).setAttribute('data-tooltip', 'Clearly conveying information and ideas through a variety of media to individuals or groups in a manner that engages the audience and helps them understand and retain the message.');
      })
    }
    this.sendSkillsArray = [...this.selectedSkillsArray]

  }
  checkClick(id) {

  
    setTimeout(() => {
      if (!(document.getElementById(id) as any).disabled) {
        if ((document.getElementById(id) as any).checked) {
          this.selectedSkillsArray.push(id)
        }
        else {
          const index = this.selectedSkillsArray.indexOf(id)
          this.selectedSkillsArray.splice(index, 1)
        }
      }

      if (this.selectedSkillsArray.length == 3) {
        this.selectedSkillsArray.forEach(disablerow => {
          (document.getElementById(disablerow) as any).disabled = true;
          let index = parseInt(disablerow.split('checkbox')[1]) - 1;
          this.skillSetArray[index].tooltip = 'You need to select at least 3 skills  for the vacancy.'

          // (document.getElementById(disablerow) as any).setAttribute('data-tooltip', 'You need to select at least 3 skills  for the vacancy.');
        })
      }
      else {
        
        this.selectedSkillsArray.forEach(disablerow => {
          (document.getElementById(disablerow) as any).disabled = false;
          let index = parseInt(disablerow.split('checkbox')[1]) - 1;

          let tooltipval = this.checkSkillSetArray[index].tooltip
          this.skillSetArray[index].tooltip = tooltipval

          // (document.getElementById(disablerow) as any).setAttribute('data-tooltip', 'Clearly conveying information and ideas through a variety of media to individuals or groups in a manner that engages the audience and helps them understand and retain the message.');
        })
      }
      this.sendSkillsArray = [...this.selectedSkillsArray]
    }, 200);

  }
  getQuestionArray(quesArray: any) {
    this.componentQuestionArray = quesArray
    console.log('quesArray: ', quesArray)
  }
  // onGetCity() {
  //   this._vacancyService.onGetCities().subscribe(data => {
  //     let result: any = data;
  //     if (result.success == true) {
  //       this.cities = result.data;
  //       console.log(this.cities);
  //     }
  //   }, (err) => {
  //     if (err.status == 400) {
  //       console.log(err.error.msg);
  //     }
  //   })
  // }
  onCopy() {
    this.isCopy = true
  }
  onAddVacancy(form: NgForm) {
    console.log('selectedSkillsArray: ', this.selectedSkillsArray)
    this.selectedSkillsArray.forEach(item => {
      let index = parseInt(item.split('checkbox')[1]) - 1;
      this.vacancyObj.skills.push(this.skillSetArray[index].name)
    })

    this.getSkillsQues(this.vacancyObj.skills.length)

    setTimeout(() => {

      this._vacancyService.onAddVacancies(this.vacancyObj).subscribe(data => {
        if (data.success == true) {
          this.getLink = data.result[0].LINK
          if (this.componentQuestionArray.length > 0) {
            this.componentQuestionArray.forEach(item => {
              let ques = {
                question: item,
                status: 0
              }
              this.questionArray.push(ques)
            })
          }

          let questionObj = {
            vacancy_id: data.result[0].V_ID,
            question: this.questionArray
          };
          this._questionService.onAddQuestion(questionObj).subscribe(data => {
            if (data.success == true) {

              this.successMsg = 'Question added successfully.';
              questionObj.question = ''
              this.firstInIt = false
              this.ngOnInit();
              this.clearQueList = true
            }
          }, e => {
            if (e.status == 400) {
              // this._toastr.error(e.error.msg, 'Error', {
              //   timeOut: 2000,
              //   positionClass: 'toast-center-center',
              //   progressBar: true,
              //   progressAnimation: 'increasing'
              // });
              console.log('Error');
            }
          })
          // this._toastr.success('Successfully registered. Please login to continue.', 'Success', {
          //   timeOut: 2000,
          //   positionClass: 'toast-center-center',
          //   progressBar: true,
          //   progressAnimation: 'increasing'
          // });
          this.successMsg = 'Vacancy added successfully.';
          document.getElementById('openCopyLinkModal').click()
          this.vacancyObj.skills = []
          setTimeout(() => {
            this.successMsg = '';

          }, 5000);
        }

      }, e => {
        if (e.status == 400) {
          // this._toastr.error(e.error.msg, 'Error', {
          //   timeOut: 2000,
          //   positionClass: 'toast-center-center',
          //   progressBar: true,
          //   progressAnimation: 'increasing'
          // });
          console.log('Error');
        }
      })

      form.resetForm();
      this.selectedSkillsArray = ['checkbox1', 'checkbox2', 'checkbox3', 'checkbox4', 'checkbox5', 'checkbox6']
      this.selectedSkillsArray.forEach(check => {
        (document.getElementById(check) as any).checked = true;
        (document.getElementById(check) as any).disabled = false;
        let index = parseInt(check.split('checkbox')[1]) - 1;
        this.skillSetArray[index].tooltip = 'Clearly conveying information and ideas through a variety of media to individuals or groups in a manner that engages the audience and helps them understand and retain the message.'

      })



      // setTimeout(() => {
      //   this.getSkillsQues(6)
      // }, 2000);
      // this.sendSkillsArray = [...this.selectedSkillsArray]
      // this.firstInIt = true
      // this.ngOnInit()
      // this.ngOnInit()

    }, 2000);
  }

  reloadCurrentPage() {
    let currentUrl = this._router.url;
    this._router.routeReuseStrategy.shouldReuseRoute = () => false;
    this._router.onSameUrlNavigation = 'reload';
    this._router.navigate([currentUrl]);

  }

  getQuestionsFromId(id) {
    this.questionArray = []
    this._vacancyService.getQuestionFromId(id).subscribe(data => {
      let result = data
      if (result.success == true) {
        this.quesTextArr = result.question
        this.quesTextArr.forEach(ele => {
          let ques = {
            question: ele.question,
            status: 1
          }
          this.questionArray.push(ques)
        })
        console.log('questionArray: ', this.questionArray)
        // this.hiddenQuestions = this.quesTextArr
        // console.log('hiddenques: ' + this.hiddenQuestions + " len: ", this.hiddenQuestions.length)
      }
    })
  }

  getSkillsQues(noOfSkill) {
    let noOfSkillset = {
      noOfSkills: noOfSkill
    }
    this.quesIdArr = []
    this._vacancyService.getQuestionFromSkills(noOfSkillset).subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.skillQuesArr = result.data;
        this.skillQuesArr.forEach(data => {
          let obj: any;
          obj = data
          if (obj.length > 1) {
            obj.forEach(item => {
              this.quesIdArr.push(item.question_id)
            });
          }
          else {
            this.quesIdArr.push(obj[0].question_id)
          }
        });
        this.quesIdobj.quesId = this.quesIdArr
        if (this.quesIdArr.length > 0) {
          this.getQuestionsFromId(this.quesIdobj)
        }
      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })
  }

  ngOnInit(): void {
    this._title.setTitle('Vacancy - Add');
    // this.onGetCity();
    this._recruiterService.getskills().subscribe(getSkills => {
      if (getSkills.success) {

        getSkills.data.forEach(item => {
          let skillsDefObj = {
            name: '',
            tooltip: ''
          }
          let checkskillsDefObj = {
            name: '',
            tooltip: ''
          }
          skillsDefObj.name = item.name
          skillsDefObj.tooltip = item.definition

          checkskillsDefObj.name = item.name
          checkskillsDefObj.tooltip = item.definition
          
          this.skillSetArray.push(skillsDefObj)
          this.checkSkillSetArray.push(checkskillsDefObj)
        })
      }
    })

    // this.skillSetArray.forEach(item => {
    //   item.tooltip = "Clearly conveying information and ideas through a variety of media to individuals or groups in a manner that engages the audience and helps them understand and retain the message."
    // })
    this.sendSkillsArray = [...this.selectedSkillsArray]
    if (this.firstInIt) {
      this.getSkillsQues(6)
      console.log('init caled again first')
    }
    console.log('init caled again')
  }
}
