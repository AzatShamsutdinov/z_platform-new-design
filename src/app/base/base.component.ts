import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {

  constructor(private _router: Router,
    private _title: Title) { }

  onAdmin() {
    this._router.navigate(['/admin/login']);
  }

  onRecruiter() {
    this._router.navigate(['/recruiter/login']);
  }
  onCandidate() {
    this._router.navigate(['/candidate/details/o83nf9LrF5KlzAQZUXws']);
  }

  ngOnInit(): void {
    this._title.setTitle('Application');
  }

}
