import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import videojs from 'video.js';
import * as adapter from 'webrtc-adapter/out/adapter_no_global.js';
import * as RecordRTC from 'recordrtc';
import * as Record from 'videojs-record/dist/videojs.record.js';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-check-equipment',
  templateUrl: './check-equipment.component.html',
  styleUrls: ['./check-equipment.component.css']
})
export class CheckEquipmentComponent implements OnInit, OnDestroy {
 
  questionId: any;
  questionIdObj: any = {
    code: ''
  };

  private _elementRef: ElementRef
  idx = 'clip1';
  private config: any;
  private player: any; 
  private plugin: any;
  
  constructor(private _title: Title, 
              private _router: Router,
              private route: ActivatedRoute,
              elementRef: ElementRef) {
    this.player = false;
    this.plugin = Record;
    this.config = {
      controls: false,
      autoplay: false,
      fluid: false,
      loop: false,
      width: 400,
      height: 300,
      bigPlayButton: false,
      controlBar: {
        volumePanel: true,
        'pictureInPictureToggle': false
      },
      plugins: {
        /*
        // wavesurfer section is only needed when recording audio-only
        wavesurfer: {
            backend: 'WebAudio',
            waveColor: '#36393b',
            progressColor: 'black',
            debug: true,
            cursorWidth: 1,
            displayMilliseconds: true,
            hideScrollbar: true,
            plugins: [
                // enable microphone plugin
                WaveSurfer.microphone.create({
                    bufferSize: 4096,
                    numberOfInputChannels: 1,
                    numberOfOutputChannels: 1,
                    constraints: {
                        video: false,
                        audio: true
                    }
                })
            ]
        },
        */
        // configure videojs-record plugin
        record: {
          audio: true,
          video: true,
          debug: true,
          maxLength: 15
        }
      }
    };
  }

  ngAfterViewInit() {
    let el = 'video_' + this.idx;
    this.player = videojs(document.getElementById(el), this.config, () => {
      this.player.record().getDevice();
      console.log('player ready! id:', el);
      var msg = 'Using video.js ' + videojs.VERSION +
        ' with videojs-record ' + videojs.getPluginVersion('record') +
        ' and recordrtc ' + RecordRTC.version;
      videojs.log(msg);
    });
    this.player.on('deviceReady', () => {
      console.log('device is ready!');
    });
    this.player.on('startRecord', () => {
      console.log('started recording!');
    });
    this.player.on('finishRecord', () => {
      console.log('finished recording: ', this.player.recordedData);
    });
    this.player.on('error', (element, error) => {
      console.warn(error);
    });

    this.player.on('deviceError', () => {
      console.error('device error:', this.player.deviceErrorCode);
    });
  }
  
  ngOnDestroy() {
    if (this.player) {
      this.player.dispose();
      this.player = false;
    }
  }

  onInterview() {
    this._router.navigate(["/candidate/question/", this.questionId]);
  }

  ngOnInit(): void {
    this._title.setTitle('Check equipment');
    this.questionId=this.route.snapshot.paramMap.get('code');
  }

  clickCheckbox(id) {
    document.getElementById(id).click()
  }

}
