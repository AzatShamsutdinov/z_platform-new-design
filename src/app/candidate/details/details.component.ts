import { Component, OnInit } from '@angular/core';
import { NgForm, FormsModule } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { CandidateService } from 'src/app/shared/candidate/candidate.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  detailsObj: any = {
    email: '',
    code: '',
    name:'',
    check:false
  };
  result: any = {};
  detailsFail = '';
  time: any = {};

  questionId: any;
  questions: any = {};
  questionIdObj: any = {
    code: ''
  };

  constructor(private _title: Title,
              private route: ActivatedRoute,
              private _candidateService: CandidateService,
              private _router: Router) { }

  onDetails(form: NgForm) {
    console.log(this.detailsObj)
    this.detailsObj.code=this.questionId;
    if (this.detailsObj) {
      this._candidateService.onCandidateDetails(this.detailsObj).subscribe(data => {
        this.result = data;
        console.log(data);
        if (this.result.success == true) {
          // console.log(data);
          
          localStorage.setItem('v_id', this.result.v_id);
          localStorage.setItem('id', this.result.c_id);
          localStorage.setItem('name', this.result.c_name);
          localStorage.setItem('token', this.result.token);
          this.time = this.questions.length * 2;
          localStorage.setItem('time', this.time);
          localStorage.setItem('question', this.questions.length);
          this._router.navigate(["/candidate/check-equipment/", this.questionIdObj.code]);
        }
      }, (err) => {
        if (err.status == 400) {
          // this._toastr.error(err.error.msg);
          this.detailsFail = 'Your details does not match ! please check your details .';
        }
      })
    }
  }

  clickCheckbox(id) {
    document.getElementById(id).click()
  }

  onGetQuestion(id) {
    this.questionIdObj.code=id;
    this._candidateService.onGetQuestion(this.questionIdObj).subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.questions = result.data;
        console.log(this.questions);
      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })
  }

  ngOnInit(): void {
    this._title.setTitle('Candidate - Details');
    this.questionId=this.route.snapshot.paramMap.get('code');
    this.onGetQuestion(this.questionId);
  }

}
