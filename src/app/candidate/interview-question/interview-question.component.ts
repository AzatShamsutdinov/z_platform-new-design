import { ActivatedRoute, Router } from '@angular/router';
import { CandidateService } from 'src/app/shared/candidate/candidate.service';
import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import videojs from 'video.js';
import * as adapter from 'webrtc-adapter/out/adapter_no_global.js';
import * as RecordRTC from 'recordrtc';
import * as Record from 'videojs-record/dist/videojs.record.js';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
declare var $: any;
@Component({
  selector: 'app-interview-question',
  templateUrl: './interview-question.component.html',
  styleUrls: ['./interview-question.component.css']
})
export class InterviewQuestionComponent implements OnInit, OnDestroy {

  recordStop = false;
  recordStart = true;
  paginationFlag: any = false
  name = '';
  noOfQue: any = {};
  answerObj: any = {
    vacancy_id: '',
    candidate_id: '',
    question_id: '',
    answer: ''
  };
  visible: Boolean = true
  p: number = 1;
  time: any = {};

  questionId: any;
  questions: any[];
  questionIdObj: any = {
    code: ''
  };
  remainingTime = false
 

  private _elementRef: ElementRef
  idx = 'clip1';
  private config: any;
  private player: any;
  private plugin: any;

  constructor(private _title: Title,
    private _router: Router,
    private _spinnerService: NgxSpinnerService,
    private _candidateService: CandidateService,
    private route: ActivatedRoute,
    elementRef: ElementRef) {
    this.player = false;
    this.plugin = Record;
    this.config = {
      controls: false,
      autoplay: false,
      fluid: false,
      loop: false,
      width: 400,
      height: 300,
      bigPlayButton: false,
      controlBar: {
        volumePanel: true,
        'pictureInPictureToggle': false
      },
      plugins: {
        /*
        // wavesurfer section is only needed when recording audio-only
        wavesurfer: {
            backend: 'WebAudio',
            waveColor: '#36393b',
            progressColor: 'black',
            debug: true,
            cursorWidth: 1,
            displayMilliseconds: true,
            hideScrollbar: true,
            plugins: [
                // enable microphone plugin
                WaveSurfer.microphone.create({
                    bufferSize: 4096,
                    numberOfInputChannels: 1,
                    numberOfOutputChannels: 1,
                    constraints: {
                        video: false,
                        audio: true
                    }
                })
            ]
        },
        */
        // configure videojs-record plugin
        record: {
          audio: true,
          video: true,
          debug: true,
          maxLength: 180
        }
      }
    };
  }

  ngAfterViewInit() {

    let el = 'video_' + this.idx;
    this.player = videojs(document.getElementById(el), this.config, () => {
      this.player.record().getDevice();
      console.log('player ready! id:', el);
      var msg = 'Using video.js ' + videojs.VERSION +
        ' with videojs-record ' + videojs.getPluginVersion('record') +
        ' and recordrtc ' + RecordRTC.version;
      videojs.log(msg);
    });
    this.player.on('deviceReady', () => {

      console.log('device is ready!');
    });
    this.player.on('startRecord', () => {

      $('#paginationDiv').hide()
      console.log('started recording!');
    });
    this.player.on('finishRecord', () => {

      $('#paginationDiv').show()

      console.log('finished recording: ', this.player.recordedData);
      this.upload(this.player.recordedData);
    });
    this.player.on('error', (element, error) => {
      console.warn(error);
    });

    this.player.on('deviceError', () => {
      console.error('device error:', this.player.deviceErrorCode);
    });
  }

  onStartRecord() {
    this.remainingTime = true
    this.visible = false
    console.log(this.visible)
    this.recordStop = true;
    this.recordStart = false;
    this.player.record().start();
  }

  onStopRecord() {
    this.visible = true
    console.log(this.visible)
    this.recordStart = true;
    this.recordStop = false;
    this.player.record().stop();    
  }

  onNextQue() {
    this.p = this.p + 1;
    if (this.noOfQue == this.p - 1) {
      this.onNext();      
    }
  }

  ngOnDestroy() {
    if (this.player) {
      this.player.dispose();
      this.player = false;
    }
  }

  hideNext() {
    // console.log("p", this.p, " noq", +this.noOfQue, " vi",this.visible)
    if (+this.noOfQue == this.p && this.visible) {
      return true
    }
    // return true
    else {
      this.visible = false
      return false
    }


  }

  upload(blob) {
    this._spinnerService.show();
    var formData = new FormData();
    formData.append('file', blob, blob.name);
    this._candidateService.onSendAnswerFile(formData).subscribe(data => {
      if (data.status == true) {
        console.log("filepath:",data.filePath);
        console.log("ques__--ID: ", this.questions[this.p - 1].ID)
        this.answerObj.vacancy_id = localStorage.getItem('v_id');
        this.answerObj.candidate_id = localStorage.getItem('id');
        this.answerObj.question_id = this.questions[this.p - 1].ID;
        this.answerObj.answer = data.filePath;
        console.log(this.answerObj);
        this._candidateService.onSendAnswer(this.answerObj).subscribe(data => {
          if (data.success == true) {
            console.log('Answer send');
            // $('#nextthankyou').hide()
            this.onNextQue();
            this._spinnerService.hide();
          }
        }, e => {
          if (e.status == 400) {
            console.log('Error');
          }
        })

      }
    }, e => {
      if (e.status == 400) {
        console.log('Error');
      }
    })
  }

  onGetQuestion(id) {
    this.questionIdObj.code = id;
    this._candidateService.onGetQuestion(this.questionIdObj).subscribe(data => {
      let result: any = data;
      if (result.success == true) {
        this.questions = result.data;
        console.log(this.questions);
      }
    }, (err) => {
      if (err.status == 400) {
        console.log(err.error.msg);
      }
    })
  }

  onNext() {
    window.location.reload();
    localStorage.clear();
    this._router.navigate(['/candidate/thank-you']);
  }

  ngOnInit(): void {
    this._title.setTitle('Interview - Question');
    this.questionId = this.route.snapshot.paramMap.get('code');
    this.onGetQuestion(this.questionId);
    this.name = localStorage.getItem('name');
    this.time = localStorage.getItem('time');
    this.noOfQue = localStorage.getItem('question');

    console.log('time:', ((parseInt(this.time) * 60) + 20))
    setTimeout(()=>{
      this.remainingTime = true
      this.onStartRecord()
    }, (20000))
    setTimeout(() => {
      localStorage.clear();
      this._router.navigate(["/candidate/thank-you"]);
    }, ((parseInt(this.time) * 60) + 20) * 1000);


    $("body").delegate(".pagination-previous", "click", function () {
      $('#paginationDiv').hide()
    });
    $("body").delegate(".pagination-next", "click", function () {

      $('#paginationDiv').hide()
    });
  }



}





