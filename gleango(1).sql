-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 18, 2021 at 09:54 AM
-- Server version: 8.0.22-0ubuntu0.20.04.3
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gleango`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `ADMIN_ID` int NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `EMAIL` varchar(50) NOT NULL,
  `USERNAME` varchar(50) NOT NULL,
  `PASSWORD` varchar(255) NOT NULL,
  `TOKEN` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`ADMIN_ID`, `NAME`, `EMAIL`, `USERNAME`, `PASSWORD`, `TOKEN`) VALUES
(1, 'Admin', 'admin@gmail.com', 'admin#112', '$2a$10$ozT7GAEH.AIYVfY0XhJYD.O6MG3zPaQLTnLAhI6mpEQlfAs7yAnw2', '\r\n\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE `candidates` (
  `C_ID` int NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `JOB_TITLE` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `COMPANY` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `EMAIL` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `PHONE` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `CITY` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `CREATED_AT` bigint NOT NULL,
  `LINK` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `TOKEN` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`C_ID`, `NAME`, `JOB_TITLE`, `COMPANY`, `EMAIL`, `PHONE`, `CITY`, `CREATED_AT`, `LINK`, `TOKEN`) VALUES
(8, 'Ajeet Mishra', 'Project Manager', 'Test Company', 'ajeet@gmail.com', '9849849849', 'Noida', 1604641366246, 'ULZS8GEM6xmzO1QLJ67YNr1ZF', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6OCwiaWF0IjoxNjEwNzkwOTMzLCJleHAiOjE2MTA3OTQ1MzN9.ZqpG9M2YoQ5eQ5pe2xuSKqqgr_8ec_Q7pz3yoj-BRRo'),
(16, 'Azat', NULL, NULL, 'ai.shamsutdinov@gmail.com', NULL, NULL, 1608295930317, 'IwtMXLZcUrEbsvHIoVmw$dGwm', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6MTYsImlhdCI6MTYwODI5ODcxNCwiZXhwIjoxNjA4MzAyMzE0fQ.hfLCr25ARZUph7y7yF22JzW-uWOyc09HLRBCyQ2YBmA'),
(35, 'Azat5', 'Web Developer', 'Test Company', 'sdf@sef.csdf', NULL, NULL, 1608620563387, 'eHKmMGgMTDzgOz1zxB#ngunio', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6MzUsImlhdCI6MTYwODYyMDU2MywiZXhwIjoxNjA4NjI0MTYzfQ.3ePSd3NFIyQXAZouciIUjOdkhNzwx1vxFCinnOzEyqY'),
(36, 'ergdf', 'PHP Developer', 'Optimise Technology', 'dfg@srf.dfg', NULL, NULL, 1608621567914, '$z$lFaHNoRJsz0b0xssxSO$8Z', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6MzYsImlhdCI6MTYwODYyMTU2NywiZXhwIjoxNjA4NjI1MTY3fQ.5Y1XGcO6c8JQjBfR60RPhUEMmFP2H6CBcFbg7RTI2-c'),
(37, '234', 'Web Developer', 'Test Company', 'sf@sdf.sdf', NULL, NULL, 1608621690717, 'zWAoh9HwIJPmuZR&Ey4JU4GLn', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6MzcsImlhdCI6MTYwODYyMTY5MCwiZXhwIjoxNjA4NjI1MjkwfQ.cpPtsq_odg846gVVkvfQ7muknLBf70qqAPJaUIN7QFc'),
(38, 'qwe', 'Web Developer', 'Test Company', 'qwe@ewq.ewq', NULL, NULL, 1608621750698, 'lqeKcKDN45NzJiHk!G4EL3ylo', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6MzgsImlhdCI6MTYwODYyMTc1MCwiZXhwIjoxNjA4NjI1MzUwfQ.VgJNaUoVwlv7ozThC0k8qFwsq_V8gKa6yXLdkanNP60'),
(41, 'Azat test', 'T1', 'C1', 'h@h.h', NULL, NULL, 1608730373813, 'sBvOfjgsk$448TSmIh1eK8sVI', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6NDEsImlhdCI6MTYwODczMDM3MywiZXhwIjoxNjA4NzMzOTczfQ.U9HWSQZGh2KczQms4cQFJuIn-qUFcSxQh6SMwAWJX5w'),
(42, 'Azat test2', 'T1', 'C1', 'j@j.j', NULL, NULL, 1608730434451, '72VL8OB#g8vwa7U5nS9LHF0kb', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6NDIsImlhdCI6MTYwODczMDQzNCwiZXhwIjoxNjA4NzM0MDM0fQ.09SlP0KzKksXd7M64g9Ab2JBBEYPDtJhnhmzKk2Dmts'),
(46, 'AzatTest', 'Test1', 'Test11', 'u@u.u', NULL, NULL, 1609138129149, 'JkIDpmJ1HcxsUc@IHrGlyZs&v', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6NDYsImlhdCI6MTYwOTEzODEyOSwiZXhwIjoxNjA5MTQxNzI5fQ.1c6OWIlv6obzWuzP6t5rMSXXD5zc41hzn4kdlps07Fg'),
(57, 'ccc', 'PHP Developer', 'Optimise Technology', 'cccc@gmail.com', NULL, NULL, 1609829787710, 'G6lbH048O09KxreGi0HjYQXfR', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6NTcsImlhdCI6MTYwOTgyOTc4NywiZXhwIjoxNjA5ODMzMzg3fQ.7XjPay6QuMQRg-Z58wyyzIkVz8Zbx9kAWadk62Rq1m4'),
(61, 'aaabbbccc', 'Test1', 'Test11', 'aaabbbccc@gmail.com', NULL, NULL, 1609832682260, 'uOxgwVMyOavQXuZv2LaKPWWrh', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6NjEsImlhdCI6MTYwOTgzMjY4MiwiZXhwIjoxNjA5ODM2MjgyfQ.r02Cn7E-uv6u8o3SKK7zFWJpXHIh0zSh7oUEpGF2Ls4'),
(62, 'bbbb', 'Test1', 'Test11', 'bbbb@gmail.com', NULL, NULL, 1609833064485, '0vALZpigpx4pFpY0Vv6Un9PwL', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6NjIsImlhdCI6MTYwOTgzMzA2NCwiZXhwIjoxNjA5ODM2NjY0fQ.E_wCcwNQiAfLB-g4Gk887MJPZQLsGCchHInTgXNL-ow'),
(86, 'aaa', 'Test Vacancy13012021', 'Test Company', 'aaa@gmail.com', NULL, NULL, 1610620698391, 'pvIb9GaZr2brvvK98FT0tz7rt', NULL),
(94, 'Azat test', 'Test_15_01', 'Test_15_011', 'o@o.o', NULL, NULL, 1610720615696, 'haDpvLbamU4OEaAD8oT72EACO', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6OTQsImlhdCI6MTYxMDcyMDYxNSwiZXhwIjoxNjEwNzI0MjE1fQ.eA2YMrJwogxjbnuAKuPeDqK-yyU_DdLjhoCtoWlxsIw'),
(95, 'Azat2', 'Test_15_01', 'Test_15_011', 'p@p.p', NULL, NULL, 1610720719893, 'xe4Dh06OzY3udngeDeUZXY4cX', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6OTUsImlhdCI6MTYxMDcyMDcxOSwiZXhwIjoxNjEwNzI0MzE5fQ.r8qghFhtor9pqijp0E6W42HvOIsMb5DnsrO3JKDhzS8'),
(97, 'test2', 'Test_15_01', 'Test_15_011', 'sdf@sdf.dsf', NULL, NULL, 1610721048399, 'kJIcSAXD6F9qRqW1t4EM1OJEG', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6OTcsImlhdCI6MTYxMDcyMTA0OCwiZXhwIjoxNjEwNzI0NjQ4fQ.7w3Zdp04nzJq789q040NfK3ON3ycsenxfg_Qe8tPSTM'),
(98, 'Test3', 'Test_15_01', 'Test_15_011', 'qwe@qqwe.ed', NULL, NULL, 1610721216615, 'DtIJcKWxcV6EpI7g3zeLqxARy', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6OTgsImlhdCI6MTYxMDcyMTIxNiwiZXhwIjoxNjEwNzI0ODE2fQ.9XZtIMhy_x9ZVG7NhLZX-GkbA6v4rn2k7IOLEqoOhjA'),
(99, 'Test3', 'Test_15_01', 'Test_15_011', 'wer@sdf.dfs', NULL, NULL, 1610721346347, 'BPRPuCCOATV1Hcw8CpZxQIrt1', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6OTksImlhdCI6MTYxMDcyMTM0NiwiZXhwIjoxNjEwNzI0OTQ2fQ.R4tijQH4ilDoWcYarBYaAwYV-P3HH_XucsW8AW_ZnQA'),
(101, 'Test5', 'Test_15_01', 'Test_15_011', 'asdf@sdf.csd', NULL, NULL, 1610721452880, 'e0T2M0g26ZSh5nMJMEoD8UGY0', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6MTAxLCJpYXQiOjE2MTA3MjE0NTIsImV4cCI6MTYxMDcyNTA1Mn0.FuFMI_1HgaMVrYEWZou3pGAZH9ZrJ_Ju0FTQNRgKE_s'),
(103, 'Test', 'Test_15_01', 'Test_15_011', 'test@test.test', NULL, NULL, 1610725105079, '8NFVgWDjkdaHhM3olfa7SH69j', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6MTAzLCJpYXQiOjE2MTA3MjUxMDUsImV4cCI6MTYxMDcyODcwNX0.9-IAJGz8ApY1NpCfde6BC_SQ3Jgjw1afq92CAAy-vE4'),
(104, 'Test10', 'Test_15_01', 'Test_15_011', 'sdf@sdf.sdf', NULL, NULL, 1610725249780, 'j3yNmR54CZ32iGLWc7ULMxPUS', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6MTA0LCJpYXQiOjE2MTA3MjUyNDksImV4cCI6MTYxMDcyODg0OX0.QkK-h6WHrhK_PaWNZ5tRkyl1SJgkUHBIP5rgeSZpc0A'),
(105, 'hghjghjghjg', 'Test Vacancy13012021', 'Test Company', 'ajeet@gmail.com', NULL, NULL, 1610782563011, 'LMEnuSfsERd9HmxnD5iXTCt2O', NULL),
(106, 'gjhhjg', 'Test Vacancy13012021', 'Test Company', 'ajeet@gmail.com', NULL, NULL, 1610782661642, 'MAy2LuqHfXGw6NWrS5E202j0l', NULL),
(107, 'dkjhsdjkdsh', 'Test Vacancy13012021', 'Test Company', 'ajeet@gmail.com', NULL, NULL, 1610784231424, 'AkkWm8xalyqJdD0qrM0SUc3ft', NULL),
(108, 'dfgdfgfd', 'Test Vacancy13012021', 'Test Company', 'ajeet@gmail.com', NULL, NULL, 1610784267107, 'aiFtloMMxNqmLhfb2yhriDbDH', NULL),
(109, 'dfgdfgfd', 'Test Vacancy13012021', 'Test Company', 'ajeet@gmail.com', NULL, NULL, 1610784267436, 'Jci8yIP9IYMLV4o7xEZTg7S9F', NULL),
(110, 'ertretre', 'Test Vacancy13012021', 'Test Company', 'ajeet@gmail.com', NULL, NULL, 1610784310802, 'VeNysmdGMKCvWGnwvKNOpBklt', NULL),
(111, 'fdgfdfd', 'Test Vacancy13012021', 'Test Company', 'ajeet@gmail.com', NULL, NULL, 1610784486082, 'yaXNAwGlkEfnfrQ3KCLwrqd4e', NULL),
(112, 'sdfsd', 'Test Vacancy13012021', 'Test Company', 'ajeet@gmail.com', NULL, NULL, 1610784530880, '0ldUsTbgTOOs7qiy1JULuqnia', NULL),
(113, 'sdfsddsf', 'Test_15_01', 'Test_15_011', 'ajeet@gmail.com', NULL, NULL, 1610788373534, '5AHWDlhGqB4HhLG0Esm8XAw4e', NULL),
(114, 'dfgfdgdf', 'Test_15_01', 'Test_15_011', 'ajeet@gmail.com', NULL, NULL, 1610788461235, 'CM5AICG5NN24EAdfRPpeenG2u', NULL),
(115, 'dfgdfgdf', 'Test_15_01', 'Test_15_011', 'ajeet@gmail.com', NULL, NULL, 1610788500891, 'C2RXk4KOHK3xY3UQwO7bVU1aE', NULL),
(116, 'dsfsdf', 'Test_15_01', 'Test_15_011', 'ajeet@gmail.com', NULL, NULL, 1610788606987, 'zCgUtDEb1znv5GardCDK1bBgK', NULL),
(118, 'jkjkhkjh', 'Test Vacancy13012021', 'Test Company', 'ajeet@gmail.com', NULL, NULL, 1610790879357, 'epybw7KQVti8oo013tqf37TvK', NULL),
(119, 'fghfghfg', 'Test Vacancy13012021', 'Test Company', 'ajeet@gmail.com', NULL, NULL, 1610790933930, '9OCFcPGC7J9LgIAg5fZdIxcZn', NULL),
(122, 'fsdfsdfds', 'Test Vacancy13012021', 'Test Company', 'ad@asd.cs', NULL, NULL, 1610795471440, 'A2MJtBl731frazvDz09mP6XRM', NULL),
(131, 'dsfsdfsd', 'Test Vacancy13012021', 'Test Company', 'df@sdf.sdf', NULL, NULL, 1610799863153, 'fUlb0XcD25wnq0f8AvD9yY309', NULL),
(132, 'cgxcdf', 'Test Vacancy13012021', 'Test Company', 'df@sdf.sdf', NULL, NULL, 1610800010778, 'ySachw5WlGc37SXeb2UmOYsU3', NULL),
(133, 'dsfdsfds', 'Test Vacancy13012021', 'Test Company', 'df@sdf.sdf', NULL, NULL, 1610800703532, 'lyb3nJayGbHO8V5CGIjTqrWQu', NULL),
(134, 'fdssdfsdf', 'Test Vacancy13012021', 'Test Company', 'df@sdf.sdf', NULL, NULL, 1610801140152, 'QbDUlH8r8UJBoh1gFgCtgFuj3', NULL),
(135, 'dsfdg', 'Test Vacancy13012021', 'Test Company', 'df@sdf.sdf', NULL, NULL, 1610801380225, 'TuiQf8MOcXH9fwTHY5VitwLEj', NULL),
(137, 'Azat1', 'Test1', 'Test1', 'q@q.q', NULL, NULL, 1610827998559, 'yc1r3CilM39Og0tEOGLsNHcPK', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjYW5kaWRhdGVJZCI6MTM3LCJpYXQiOjE2MTA4Mjc5OTgsImV4cCI6MTYxMDgzMTU5OH0.kihDMV-53WnzBsBfjRFUJlLinckBb-ljg0hbUAJeKBA');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `ID` int NOT NULL,
  `CITY_NAME` varchar(255) NOT NULL,
  `STATUS` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`ID`, `CITY_NAME`, `STATUS`) VALUES
(1, 'Kolkata', 1),
(2, 'Rajasthan', 1),
(3, 'Delhi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `ID` int NOT NULL,
  `V_ID` int NOT NULL,
  `QUESTION` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`ID`, `V_ID`, `QUESTION`) VALUES
(1, 1, 'First Question'),
(2, 1, 'Second Question'),
(3, 1, 'Third Question'),
(4, 2, 'First question'),
(5, 2, 'Second question'),
(6, 2, 'Third question'),
(8, 3, 'First question'),
(9, 3, 'Second question'),
(10, 3, 'Third question'),
(11, 3, 'Fourth question');

-- --------------------------------------------------------

--
-- Table structure for table `question_answer`
--

CREATE TABLE `question_answer` (
  `ID` int NOT NULL,
  `V_ID` int NOT NULL,
  `C_ID` int NOT NULL,
  `Q_ID` int NOT NULL,
  `ANSWER` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `question_answer`
--

INSERT INTO `question_answer` (`ID`, `V_ID`, `C_ID`, `Q_ID`, `ANSWER`) VALUES
(1, 1, 63, 2, 'uploads/1610540444928.mkv'),
(2, 1, 63, 3, 'uploads/1610540468863.mkv'),
(3, 1, 64, 1, 'uploads/1610540695117.mkv'),
(4, 1, 64, 2, 'uploads/1610540710783.mkv'),
(5, 1, 64, 3, 'uploads/1610540719898.mkv'),
(6, 1, 65, 1, 'uploads/1610540867788.mkv'),
(7, 1, 66, 1, 'uploads/1610540981070.mkv'),
(8, 1, 66, 3, 'uploads/1610540996264.mkv'),
(9, 1, 67, 1, 'uploads/1610541174895.mkv'),
(10, 1, 67, 3, 'uploads/1610541207840.mkv'),
(11, 1, 71, 1, 'uploads/1610545324219.mkv'),
(12, 1, 71, 2, 'uploads/1610544834110.mkv'),
(13, 1, 72, 1, 'uploads/1610546561254.mkv'),
(14, 1, 72, 2, 'uploads/1610546573170.mkv'),
(15, 1, 73, 1, 'uploads/1610603546391.mkv'),
(16, 1, 73, 2, 'uploads/1610603548912.mkv'),
(17, 1, 73, 3, 'uploads/1610603553039.mkv'),
(18, 1, 74, 1, 'uploads/1610603940968.mkv'),
(19, 1, 74, 2, 'uploads/1610603946320.mkv'),
(20, 1, 75, 1, 'uploads/1610608982692.mkv'),
(21, 1, 75, 2, 'uploads/1610609003934.mkv'),
(22, 1, 76, 1, 'uploads/1610612344465.mkv'),
(23, 1, 76, 2, 'uploads/1610612348657.mkv'),
(24, 1, 76, 3, 'uploads/1610612359409.mkv'),
(25, 1, 77, 1, 'uploads/1610614010756.mkv'),
(26, 1, 77, 2, 'uploads/1610614044432.mkv'),
(27, 1, 77, 3, 'uploads/1610614054602.mkv'),
(28, 1, 78, 1, 'uploads/1610614782100.mkv'),
(29, 1, 78, 2, 'uploads/1610614784629.mkv'),
(30, 1, 78, 3, 'uploads/1610614792778.mkv'),
(31, 1, 79, 1, 'uploads/1610615714188.mkv'),
(32, 1, 79, 2, 'uploads/1610615727819.mkv'),
(33, 1, 79, 3, 'uploads/1610615749567.mkv'),
(34, 1, 80, 1, 'uploads/1610615897222.mkv'),
(35, 1, 80, 3, 'uploads/1610615906346.mkv'),
(36, 1, 81, 1, 'uploads/1610616113164.mkv'),
(37, 1, 81, 2, 'uploads/1610616115273.mkv'),
(38, 1, 83, 1, 'uploads/1610616743302.mkv'),
(39, 1, 83, 2, 'uploads/1610616745903.mkv'),
(40, 1, 83, 3, 'uploads/1610616756420.mkv'),
(41, 1, 84, 1, 'uploads/1610619350516.mkv'),
(42, 1, 84, 2, 'uploads/1610619353063.mkv'),
(43, 1, 84, 3, 'uploads/1610619359553.mkv'),
(44, 1, 85, 1, 'uploads/1610620610732.mkv'),
(45, 1, 85, 2, 'uploads/1610620615140.mkv'),
(46, 1, 85, 3, 'uploads/1610620623451.mkv'),
(47, 1, 56, 1, 'uploads/1610620701817.mkv'),
(48, 1, 56, 2, 'uploads/1610620705086.mkv'),
(49, 1, 87, 1, 'uploads/1610621215967.mkv'),
(50, 1, 87, 2, 'uploads/1610621218107.mkv'),
(51, 1, 87, 3, 'uploads/1610621219921.mkv'),
(52, 1, 88, 1, 'uploads/1610622168458.mkv'),
(53, 1, 89, 1, 'uploads/1610622652601.mkv'),
(54, 1, 89, 3, 'uploads/1610622666478.mkv'),
(55, 1, 90, 1, 'uploads/1610623166710.mkv'),
(56, 1, 90, 2, 'uploads/1610623173178.mkv'),
(57, 1, 90, 3, 'uploads/1610623177093.mkv'),
(58, 1, 91, 1, 'uploads/1610623398492.mkv'),
(59, 1, 92, 1, 'uploads/1610624726096.mkv'),
(60, 1, 92, 2, 'uploads/1610624732145.mkv'),
(61, 1, 92, 3, 'uploads/1610624735642.mkv'),
(62, 1, 93, 1, 'uploads/1610624934697.mkv'),
(63, 1, 93, 2, 'uploads/1610624936653.mkv'),
(64, 1, 93, 3, 'uploads/1610624938296.mkv'),
(65, 2, 94, 4, 'uploads/1610720647214.mkv'),
(66, 2, 94, 5, 'uploads/1610720656386.mkv'),
(67, 2, 94, 6, 'uploads/1610720663400.mkv'),
(68, 2, 95, 4, 'uploads/1610720735583.mkv'),
(69, 2, 95, 5, 'uploads/1610720746560.mkv'),
(70, 2, 95, 6, 'uploads/1610720757511.mkv'),
(71, 2, 96, 5, 'uploads/1610721031855.mkv'),
(72, 2, 96, 6, 'uploads/1610721034961.mkv'),
(73, 2, 97, 4, 'uploads/1610721058760.mkv'),
(74, 2, 97, 5, 'uploads/1610721068532.mkv'),
(75, 2, 97, 6, 'uploads/1610721084722.mkv'),
(76, 2, 98, 4, 'uploads/1610721250552.mkv'),
(77, 2, 98, 5, 'uploads/1610721266205.mkv'),
(78, 2, 98, 6, 'uploads/1610721284007.mkv'),
(79, 2, 99, 4, 'uploads/1610721356591.mkv'),
(80, 2, 99, 5, 'uploads/1610721362344.mkv'),
(81, 2, 101, 4, 'uploads/1610721459344.mkv'),
(82, 2, 102, 5, 'uploads/1610722982831.mkv'),
(83, 2, 103, 5, 'uploads/1610725119114.mkv'),
(84, 2, 103, 6, 'uploads/1610725126315.mkv'),
(85, 2, 104, 5, 'uploads/1610725269002.mkv'),
(86, 2, 104, 4, 'uploads/1610725263017.mkv'),
(87, 2, 104, 6, 'uploads/1610725284904.mkv'),
(88, 1, 8, 2, 'uploads/1610839614738.mkv'),
(89, 1, 8, 3, 'uploads/1610839617194.mkv'),
(90, 2, 8, 5, 'uploads/1610837502044.mkv'),
(91, 2, 8, 6, 'uploads/1610837507398.mkv'),
(92, 2, 8, 7, 'uploads/1610837510695.mkv'),
(93, 1, 117, 1, 'uploads/1610790213750.mkv'),
(94, 1, 117, 3, 'uploads/1610790219293.mkv'),
(95, 1, 120, 2, 'uploads/1610791198984.webm'),
(96, 1, 120, 3, 'uploads/1610791203630.webm'),
(97, 1, 121, 2, 'uploads/1610791649824.webm'),
(98, 1, 121, 3, 'uploads/1610791662691.webm'),
(131, 1, 136, 1, 'uploads/1610803423327.mkv'),
(132, 1, 136, 2, 'uploads/1610803428919.mkv'),
(133, 1, 136, 3, 'uploads/1610803433314.mkv'),
(134, 3, 137, 8, 'uploads/1610828018276.mkv'),
(135, 3, 137, 9, 'uploads/1610828024602.mkv'),
(136, 3, 137, 10, 'uploads/1610828030094.mkv'),
(137, 3, 137, 11, 'uploads/1610828033942.mkv');

-- --------------------------------------------------------

--
-- Table structure for table `recruiters`
--

CREATE TABLE `recruiters` (
  `ID` int NOT NULL,
  `USER_ID` int NOT NULL,
  `PHONE` varchar(20) NOT NULL,
  `COMPANY` varchar(255) NOT NULL,
  `CITY` varchar(100) NOT NULL,
  `STATUS` tinyint(1) NOT NULL,
  `CREATED_AT` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recruiters`
--

INSERT INTO `recruiters` (`ID`, `USER_ID`, `PHONE`, `COMPANY`, `CITY`, `STATUS`, `CREATED_AT`) VALUES
(1, 4, '986589658', 'Test Company', 'Kolkata', 1, 1604116001538),
(4, 2, '9898989898', 'Proptimise Technology', 'Rajasthan', 1, 1604116001538),
(11, 13, '123123123', 'Test company', 'Test city', 1, 1608017503847),
(12, 14, '123', 'Company23', 'City2', 1, 1608620075898),
(13, 15, '123', 'Qwe', 'Qwe', 1, 1609137772907),
(14, 16, '123', 'Q', 'Q', 1, 1609137951681),
(15, 17, '9876543210', 'abc', 'abc', 1, 1609831745483),
(16, 18, '321', 'I', 'I', 1, 1610720499987),
(17, 19, '457345', 'W', 'W', 1, 1610827878361);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` bigint NOT NULL,
  `ROLE` enum('1','2','3') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '1=> Admin , 2=> Recruiter, 3=> Candidate',
  `FULLNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `EMAIL` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `USERNAME` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `PASSWORD` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TOKEN` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `STATUS` tinyint(1) NOT NULL DEFAULT '1',
  `CREATED_AT` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `ROLE`, `FULLNAME`, `EMAIL`, `USERNAME`, `PASSWORD`, `TOKEN`, `STATUS`, `CREATED_AT`) VALUES
(1, '1', 'Admin', 'admin@gmail.com', 'admin#112', '$2a$10$ozT7GAEH.AIYVfY0XhJYD.O6MG3zPaQLTnLAhI6mpEQlfAs7yAnw2', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsInVzZXJSb2xlIjoiMSIsImlhdCI6MTYxMDk2MzUwNywiZXhwIjoxNjEwOTY3MTA3fQ.2QjleH1x0Y-wOT-DBiAAadEmWKAhlotHCXCrn4WrtXo', 1, 20201030072050),
(2, '2', 'Ajeet Mishra', 'ajeet@gmail.com', 'ajeet#112', '$2a$10$ozT7GAEH.AIYVfY0XhJYD.O6MG3zPaQLTnLAhI6mpEQlfAs7yAnw2', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjIsInVzZXJSb2xlIjoiMiIsImlhdCI6MTYxMDc5NDgzOCwiZXhwIjoxNjEwNzk4NDM4fQ.GwPqDJlO2cPdRynfANfJVegWDC-LjqbeNjQJUwZlFFg', 1, 20201030072226),
(4, '2', 'Somnath', 'somnath@gmail.com', 'somnath#112', '$2a$10$Zfh2UjGqJpL4wDOwxpHzDOlOvuph.bHrdIXFlL5aKFgmVWq8p2onK', NULL, 1, 1604116001535),
(13, '2', 'Azat', 'ai.shamsutdinov@gmail.com', 'Azat', '$2a$10$3NV0cvbsXchX6oEGip7MeeHJB9pOgUEwG3zKpJJMwQ0u6ABQL7b5e', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEzLCJ1c2VyUm9sZSI6IjIiLCJpYXQiOjE2MTA2MjQ4ODgsImV4cCI6MTYxMDYyODQ4OH0.hJ-aGKHfQDyN3PPJ7n0It69MHRtriHSECRl6fr1RTmI', 1, 1608017503843),
(14, '2', 'Azat 2', 'ai.shamsutdinov@gmail.com', 'Azat2', '$2a$10$lrose0kS2DNyNaJxD2vkfu6r64oHNX7fjQeBINMA0ddVgEUpCr0sy', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjE0LCJ1c2VyUm9sZSI6IjIiLCJpYXQiOjE2MDg2MjEzNDIsImV4cCI6MTYwODYyNDk0Mn0.0prTiTKMPRjEPgRUdyIU_0LOmnM5HduNooHDjVbZkeo', 1, 1608620075894),
(15, '2', 'Azat3', 'qwe@reg.wf', 'azat3', '$2a$10$4RJPLpqEan3efwoEVn.GP.0qBCd5//Z7xji3chNODSZVWiHsuhV4a', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjE1LCJ1c2VyUm9sZSI6IjIiLCJpYXQiOjE2MDkxMzc4ODQsImV4cCI6MTYwOTE0MTQ4NH0.VzKeKpw2S42jbN-GTcEL872KXgbwybpLokfG48wkaPI', 1, 1609137772902),
(16, '2', 'Azat', 'r@r.r', 'Azat7', '$2a$10$hpMpYbOWWm4dVjfox9j5DO65FinqLpuALOBOmZh0.N8iFS2nfh0yW', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjE2LCJ1c2VyUm9sZSI6IjIiLCJpYXQiOjE2MTA4MDM0OTUsImV4cCI6MTYxMDgwNzA5NX0.pOzVcyy-7Ap9kHwMaaszNNk_zyZhe5bZS1j65cYOstU', 1, 1609137951678),
(17, '2', 'Anirban Roy', 'anir@gmail.com', 'anir9', '$2a$10$bYmShpGY8.lSlWPOkuv5JuPMvfx0VuPjlOs6YtokKiu8UtIzRAQHm', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjE3LCJ1c2VyUm9sZSI6IjIiLCJpYXQiOjE2MDk4MzU2ODMsImV4cCI6MTYwOTgzOTI4M30.IVBcQejeQfuT4nng7g1MNnT8-gHbrvvU4FWxiK4n55M', 1, 1609831745480),
(18, '2', 'Azat', 'i@i.i', 'Azat_15_01', '$2a$10$Pu5X/Cq49CcrzjUCox5E/.6UKSZhBIaUMp.kr.ZClFMqZXqmAg2YG', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjE4LCJ1c2VyUm9sZSI6IjIiLCJpYXQiOjE2MTA3MjUzMDIsImV4cCI6MTYxMDcyODkwMn0.7yGuAUpI7p6MiQTpF17Vgoz9fIoYULRJCZmFktgXs-A', 1, 1610720499983),
(19, '2', 'Azat', 'asd@sdf.gfd', 'azat_16_01', '$2a$10$KEQUtIo1AX0IQv9.VeRTQ.AUsRfKUdTJQyAIAPd3vlAlwPVUpAba.', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjE5LCJ1c2VyUm9sZSI6IjIiLCJpYXQiOjE2MTA4MjgwNTUsImV4cCI6MTYxMDgzMTY1NX0.5DIUYeOckA7D_hqHGjj8vXrjri9AwwQ_vBSadAKWI-E', 1, 1610827878357);

-- --------------------------------------------------------

--
-- Table structure for table `vacancy`
--

CREATE TABLE `vacancy` (
  `V_ID` int NOT NULL,
  `R_ID` int NOT NULL,
  `TITLE` varchar(255) NOT NULL,
  `COMPANY` varchar(255) NOT NULL,
  `CITY` varchar(100) NOT NULL,
  `LINK` varchar(255) NOT NULL,
  `CREATED_AT` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacancy`
--

INSERT INTO `vacancy` (`V_ID`, `R_ID`, `TITLE`, `COMPANY`, `CITY`, `LINK`, `CREATED_AT`) VALUES
(1, 14, 'Test Vacancy13012021', 'Test Company', 'Test City', '3QLVanHXqOQdDRBw2wid', 1610540183807),
(2, 16, 'Test_15_01', 'Test_15_011', 'Test_15_0111', 'o7megyRJS9sCZX7Ag7tK', 1610720544898),
(3, 17, 'Test1', 'Test1', 'Test1', 'zfq2woy0jtTY08rJryhA', 1610827919648);

-- --------------------------------------------------------

--
-- Table structure for table `vacancy_candidate`
--

CREATE TABLE `vacancy_candidate` (
  `VC_ID` int NOT NULL,
  `VAC_ID` int NOT NULL,
  `CAN_ID` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vacancy_candidate`
--

INSERT INTO `vacancy_candidate` (`VC_ID`, `VAC_ID`, `CAN_ID`) VALUES
(1, 1, 63),
(2, 1, 64),
(3, 1, 65),
(4, 1, 66),
(5, 1, 67),
(6, 1, 68),
(7, 1, 69),
(8, 1, 70),
(9, 1, 71),
(10, 1, 72),
(11, 1, 73),
(12, 1, 74),
(13, 1, 75),
(14, 1, 76),
(15, 1, 77),
(16, 1, 78),
(17, 1, 79),
(18, 1, 80),
(19, 1, 81),
(20, 1, 82),
(21, 1, 83),
(22, 1, 84),
(23, 1, 85),
(24, 1, 56),
(25, 1, 87),
(26, 1, 88),
(27, 1, 89),
(28, 1, 90),
(29, 1, 91),
(30, 1, 92),
(31, 1, 93),
(32, 2, 94),
(33, 2, 95),
(34, 2, 96),
(35, 2, 97),
(36, 2, 98),
(37, 2, 99),
(38, 2, 100),
(39, 2, 101),
(40, 2, 102),
(41, 2, 103),
(42, 2, 104),
(43, 1, 8),
(44, 1, 8),
(45, 1, 8),
(46, 1, 8),
(47, 1, 8),
(48, 1, 8),
(49, 1, 8),
(50, 1, 8),
(51, 2, 8),
(52, 2, 8),
(53, 2, 8),
(54, 2, 8),
(55, 1, 117),
(56, 1, 8),
(57, 1, 8),
(58, 1, 120),
(59, 1, 121),
(60, 1, 100),
(61, 1, 102),
(62, 1, 123),
(63, 1, 123),
(64, 1, 124),
(65, 1, 125),
(66, 1, 125),
(67, 1, 126),
(68, 1, 126),
(69, 1, 127),
(70, 1, 127),
(71, 1, 128),
(72, 1, 129),
(73, 1, 130),
(74, 1, 136),
(75, 3, 137);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`ADMIN_ID`);

--
-- Indexes for table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`C_ID`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `V_ID` (`V_ID`);

--
-- Indexes for table `question_answer`
--
ALTER TABLE `question_answer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `recruiters`
--
ALTER TABLE `recruiters`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `USER_ID` (`USER_ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `vacancy`
--
ALTER TABLE `vacancy`
  ADD PRIMARY KEY (`V_ID`),
  ADD KEY `LINK` (`LINK`),
  ADD KEY `R_ID` (`R_ID`);

--
-- Indexes for table `vacancy_candidate`
--
ALTER TABLE `vacancy_candidate`
  ADD PRIMARY KEY (`VC_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `ADMIN_ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `C_ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `question_answer`
--
ALTER TABLE `question_answer`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `recruiters`
--
ALTER TABLE `recruiters`
  MODIFY `ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` bigint NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `vacancy`
--
ALTER TABLE `vacancy`
  MODIFY `V_ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vacancy_candidate`
--
ALTER TABLE `vacancy_candidate`
  MODIFY `VC_ID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
