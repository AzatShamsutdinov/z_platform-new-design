const express = require('express')
const dotenv = require('dotenv')
const cors = require('cors')
const path = require('path')

const https = require('https')
const fs = require('fs')

//initialize express
const app = express()

//initialize environment values
dotenv.config()
//initialize CORS finction
app.use(cors())
app.use(express.urlencoded({extended:false}));


//Set app default port
app.set('port', process.env.APP_PORT || 5050)
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));


//Admin Route for app
const appAdminRouter = require('./routes/app/adminRoute');
app.use('/admin', appAdminRouter)

//Recruiter Route for app
const appRecruiterRouter = require('./routes/app/recruiterRoute');
app.use('/recruiter', appRecruiterRouter)

//Candidate Route for app
const appCandidateRouter = require('./routes/app/candidateRoute');
app.use('/candidate', appCandidateRouter)

//Server connection
// app.listen(app.get('port'), () => {
//     console.log("Server listening on port " + app.get('port'))
// })




 const options = {
    cert: fs.readFileSync('/etc/ssl/www_platform_gleango_com.crt'),
    key: fs.readFileSync('/etc/ssl/www_platform_gleango_com.key')
 }

https.createServer(options, app).listen(app.get('port'), () => {
     console.log("Express https server listening on port " + app.get('port'))
})
