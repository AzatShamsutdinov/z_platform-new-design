const fs = require('fs');
const SpeechToTextV1 = require('ibm-watson/speech-to-text/v1');
const { IamAuthenticator } = require('ibm-watson/auth');
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);

var converterAI = {
  convertSpeechToText: function (filePath) {
    // ffmpeg.setFfmpegPath("D:/ffmpeg/bin/ffmpeg.exe")
    var res;
    console.log('filepath: spc: ',filePath )

    ffmpeg(filePath).toFormat('wav')
      .on('end', function (e) {
        console.log('done', e)
      }).on('error', function (error) {
        console.log('error', error)
      })
      .saveToFile('./audio-uploads/newAudio.wav');

    const speechToText = new SpeechToTextV1({
      authenticator: new IamAuthenticator({ apikey: 'DrjrDLddYo66D0RpiVyED5H9sXqK_ZyHULuZmBLyIBp2' }),
      serviceUrl: 'https://api.us-east.speech-to-text.watson.cloud.ibm.com/instances/131cea69-5327-494b-ac31-57827c1f2380'
    });

    var promise = new Promise(function (resolve, reject) {
      setTimeout(function () {

        const params = {
          // From file
          audio: fs.createReadStream('./audio-uploads/newAudio.wav'),
          contentType: 'audio/wav'
        };

        speechToText.recognize(params)
          .then(response => {
            res = JSON.stringify(response.result)
            console.log("res: ", JSON.stringify(response.result))
            resolve(res)
            // return res
          })
          .catch(err => {
            console.log(err);
            reject(err)
          });

        // or streaming
        fs.createReadStream('./audio-uploads/newAudio.wav')
          .pipe(speechToText.recognizeUsingWebSocket({ contentType: 'audio/wav' }))
          .pipe(fs.createWriteStream('./transcription.txt'));


      },1000);
    });

    return promise

  }
 }

// var abc = converterAI.convertSpeechToText('uploads/1640586396162.mkv').then(r=> {console.log('abc',r)}).catch(e => console.log(e))
// console.log('abc: ', abc)

module.exports = converterAI;





