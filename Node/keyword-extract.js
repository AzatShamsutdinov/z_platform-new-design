const keyword_extractor = require("keyword-extractor");


var keywordExtract = {
    extractor: function (sentence, keywords) {
        // sentence = "President Obama woke up Monday facing a Congressional defeat that many in both parties believed could hobble his presidency."

        //  Extract the keywords
        const extraction_result =
            keyword_extractor.extract(sentence, {
                language: "english",
                remove_digits: true,
                return_changed_case: true,
                remove_duplicates: false
            });

        // const keywords = ['woke', 'presidency', 'tuesday', 'monday']
        const extractedWords = []
        for (words of keywords) {
            if (extraction_result.includes(words)) {
                console.log("extraction_result:", words)
                extractedWords.push(words)
            }
            else {
                console.log('not found', words)
            }
        }

        console.log('result: ', extractedWords)
        return extractedWords

    }
}

module.exports = keywordExtract;

// var abc = keywordExtract.extractor("President Obama woke up Monday facing a Congressional defeat that many in both parties believed could hobble his presidency.", ['obama','presidency','tues', 'monday']).then(r=> {console.log('abc',r)}).catch(e => console.log(e))
