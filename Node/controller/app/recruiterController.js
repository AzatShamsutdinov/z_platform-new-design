const async = require("async");
const _db = require("../../database/db_connect");
const utils = require("../../services/utils");
const jwt = require("jsonwebtoken");

const accountSid = "AC5aa1da43d62860c1efd6930a37109e38";
const authToken = "b2b8990ef305aa74c992b9f9356dc5cb";
const JWT_SECRET = process.env.TOKEN_SECRET;
const twilioClient = require("twilio")(accountSid, authToken);
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(
    "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"
);

const { v4: uuid } = require("uuid");

module.exports = {
    login: (req, res) => {
        if (!req.body.username  || !req.body.password)
            return res.status(400).send({ msg: "Required field is missing" });
        async.auto(
            {
                findEntity: (cb) => {
                    let sql =
                        "SELECT * FROM users WHERE USERNAME ='" +
                        req.body.username +
                        "' or EMAIL ='" + req.body.username +"' and ROLE=2";
                    _db.query(sql, function (err, userDoc, fields) {
                        if (err) throw err;

                        if (!userDoc[0]) return cb({ msg: "Username does not exist" });
                        if (!utils.comparePassword(req.body.password, userDoc[0].PASSWORD))
                            return cb({ msg: "Invalid password" });
                        return cb(null, userDoc[0]);
                    });
                },
                generateToken: [
                    "findEntity",
                    function (results, cb) {
                        let accessToken = jwt.sign(
                            {
                                userId: results.findEntity.ID,
                                userRole: results.findEntity.ROLE,
                            },
                            process.env.TOKEN_SECRET,
                            { expiresIn: "1h" }
                        );

                        return cb(null, accessToken);
                    },
                ],
                updateToken: [
                    "generateToken",
                    function (results, cb) {
                        let updateSql =
                            "UPDATE users SET TOKEN = '" +
                            results.generateToken +
                            "' WHERE ID = '" +
                            results.findEntity.ID +
                            "'";
                        _db.query(updateSql, function (err, result) {
                            if (err) throw err;
                            return cb(null, result);
                        });
                    },
                ],
            },
            (err, results) => {
                if (err) return res.status(400).send(err);
                return res
                    .status(200)
                    .send({
                        success: true,
                        recruiter: results.findEntity.USERNAME,
                        uid: results.findEntity.ID,
                        token: results.generateToken,
                    });
            }
        );
    }, //End of Login Function

    sendEmailVerificationCode: (req, res) => {
        try {
            let sql = "SELECT * FROM users WHERE EMAIL ='" + req.body.email + "'";
            _db.query(sql, function (err, result) {
                if (err) throw err;

                if (result.length > 0) {
                    let id = uuid();
                    const secret = JWT_SECRET + result[0].PASSWORD;
                    const paylod = {
                        email: req.body.email,
                        id: result[0].ID,
                    };
                    const token = jwt.sign(paylod, secret, { expiresIn: "1h" });

                    const content = {
                        to: req.body.email,
                        from: "e.regina@gleango.com",
                        subject: "Password Recovery",
                        html: `<body>
                                <p>Click to set a new password : <a href="https://platform.gleango.com/#/recruiter/newpassword/${result[0].ID}/${token}">Reset password</a></p>

                                </body>`,
                    };
                    sgMail.send(content);
                    return res
                        .status(200)
                        .send({
                            sucess: true,
                            message: "Please check your email for the next step",
                        });
                } else {
                    return res
                        .status(400)
                        .send({
                            sucess: false,
                            message: "User does not exist.",
                        });
                }
            });

            // twilioClient.verify
            //     .services("VAb7763828dff60d13232bd6543c2ac8ce") //Put the Verification service SID here
            //     .verifications.create({
            //         channelConfiguration: {
            //             template_id: 'd-5dae37c010a8483f99e264d6885aed9c',
            //             from: 'webbybutter.ravi@gmail.com',
            //         }, to: req.body.email, channel: "email"
            //     })
            //     .then(verification => {
            //         console.log(verification.sid);
            //         res.send(verification.sid)
            //     });
        } catch (error) {
            res.send(error.message);
        }
    },

    verfiyEmailVerificationCode: (req, res) => {
        // twilioClient.verify
        //     .services("VAb7763828dff60d13232bd6543c2ac8ce") //Put the Verification service SID here
        //     .verificationChecks.create({ to: req.body.email, code: req.body.code })
        //     .then(verification_code => {
        //         res.send(verification_code.status)
        //         console.log(verification_code.status);
        //     });

        const { id, token } = req.params;
        const { password } = req.body;
        try {
            let sql = "SELECT * FROM users WHERE ID ='" + id + "'";
            _db.query(sql, function (err, result) {
                if (err) throw err;
                if (result.length > 0) {
                    const secret = JWT_SECRET + result[0].PASSWORD;
                    try {
                        const payload = jwt.verify(token, secret);

                        if (payload) {
                            var upd_password = utils.hashPassword(password);
                            let sql2 = `UPDATE users SET PASSWORD="${upd_password}" WHERE ID =${id}`;
                            _db.query(sql2, function (err, result2) {
                                if (err) throw err;
                                // res.status(200).send(result2)
                                if (result2) {
                                    console.log("Password Changed!");
                                    res.send({ sucess: true, message: "Password Changed!" });
                                }
                            });
                        } else {
                            res.status(400).send({ sucess: false, message: "Token Expired." });
                        }
                    }
                    catch (error) { 
                        return res.status(400).send({sucess: false, message: "Given Link is expired." })
                    }

                } else {
                    return res.status(400).send({sucess: false, message: "User does not exist in our records" });
                }
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    getUsersDetail: (req, res) => {
        try {
            console.log("detail: ", req.admin.ID);
            let sql =
                "SELECT * FROM users WHERE STATUS=1 AND ID = '" + req.admin.ID + "'";
            _db.query(sql, function (err, userDoc) {
                if (err) throw err;
                res.status(200).send({ success: true, data: userDoc });
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    getallcities: async (req, res) => {
        try {
            let sql = "SELECT * FROM city WHERE STATUS=1";
            _db.query(sql, function (err, userDoc, fields) {
                if (err) throw err;
                res.status(200).send({ success: true, data: userDoc });
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    getskills: async (req, res) => {
        try {
            let sql = "SELECT * FROM skill";
            _db.query(sql, function (err, userDoc) {
                if (err) throw err;
                res.status(200).send({ success: true, data: userDoc });
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    getscores: async (req, res) => {
        try {
            let sql = "SELECT * FROM score";
            _db.query(sql, function (err, userDoc) {
                if (err) throw err;
                res.status(200).send({ success: true, data: userDoc });
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    addcandidate: async (req, res) => {
        try {
            let resultstr = "";
            let characters =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            let charactersLength = characters.length;
            for (var i = 0; i < 25; i++) {
                resultstr += characters.charAt(
                    Math.floor(Math.random() * charactersLength)
                );
            }

            let sql =
                "INSERT INTO candidates (NAME, JOB_TITLE, COMPANY, EMAIL, PHONE, CITY, CREATED_AT, LINK) VALUES ?";
            let values = Array([
                req.body.name,
                req.body.job_title,
                req.body.company,
                req.body.email,
                req.body.phone,
                req.body.city,
                new Date().getTime(),
                resultstr,
            ]);

            await _db.query(sql, [values], function (err, result) {
                if (err) throw err;
                res
                    .status(200)
                    .send({ success: true, msg: "Candidate added successfully." });
            });
        } catch (err) {
            res.status(400).send(err);
            console.log(err);
        }
    },

    updatecandidate: async (req, res) => {
        try {
            let sql =
                "UPDATE candidates SET NAME = '" +
                req.body.name +
                "' , JOB_TITLE = '" +
                req.body.job_title +
                "' , COMPANY = '" +
                req.body.company +
                "', EMAIL = '" +
                req.body.email +
                "', PHONE = '" +
                req.body.phone +
                "', CITY = '" +
                req.body.city +
                "' WHERE C_ID='" +
                req.body.id +
                "'  ";
            await _db.query(sql, function (err, result) {
                if (err) throw err;
                res
                    .status(200)
                    .send({ success: true, msg: "Candidate updated successfully." });
            });
        } catch (err) {
            res.status(400).send(err);
            console.log(err);
        }
    },

    deletecandidate: async (req, res) => {
        try {
            let sql = "DELETE FROM candidates WHERE C_ID = '" + req.body.id + "'";
            await _db.query(sql, function (err, fields) {
                if (err) throw err;
                res
                    .status(200)
                    .send({ success: true, msg: "Vacancy deleted Succefully.." });
            });
        } catch (err) {
            res.status(400).send(err);
        }
    },
    getcandidate: async (req, res) => {
        try {
            let sql =
                "SELECT ca.* FROM candidates as ca WHERE ca.C_ID = '" +
                req.body.id +
                "'";
            await _db.query(sql, function (err, userDoc) {
                if (err) throw err;
                res.status(200).send({ success: true, data: userDoc });
            });
        } catch (err) {
            res.status(400).send(err);
        }
    },

    getallcandidates: async (req, res) => {
        try {
            console.log("user ID", req.admin.ID);
            let sql =
                "SELECT * FROM candidates WHERE C_ID IN (SELECT CAN_ID FROM vacancy_candidate,vacancy,recruiters  WHERE vacancy_candidate.VAC_ID = vacancy.V_ID AND recruiters.ID= vacancy.R_ID AND recruiters.USER_ID='" +
                req.admin.ID +
                "')";
            // let sql = "SELECT ca.* FROM candidates as ca";
            _db.query(sql, function (err, userDoc, fields) {
                if (err) throw err;
                //console.log(userDoc)
                res.status(200).send({ success: true, data: userDoc });
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    addvacancy: async (req, res) => {
        try {
            let latestVacancy = [];
            let sql = "SELECT * FROM recruiters WHERE USER_ID='" + req.admin.ID + "'";
            await _db.query(sql, async (err, userDoc) => {
                if (err) throw err;
                let sql =
                    "INSERT INTO vacancy (R_ID,TITLE,COMPANY,CITY,LINK,CREATED_AT) VALUES ?";
                let values = Array([
                    userDoc[0].ID,
                    req.body.title,
                    req.body.company,
                    req.body.city,
                    utils.getRandomCode(20),
                    new Date().getTime(),
                ]);

                await _db.query(sql, [values], async (err, result) => {
                    if (err) throw err;
                    let sql =
                        "SELECT * FROM vacancy WHERE V_ID = (SELECT max(V_ID) FROM vacancy)";
                    // var skills = []
                    // skills = req.body.skills
                    var newVal = req.body.skills.join();
                    let sql2 = `INSERT INTO skills_selected SET V_ID = (SELECT V_ID FROM vacancy WHERE V_ID = (SELECT max(V_ID) FROM vacancy)), skills = '${newVal}'`;

                    await _db.query(sql, async (err, result1) => {
                        if (err) throw err;
                        console.log("skill arr: ", newVal);
                        var newValues = [req.body.skills];

                        await _db.query(sql2, [newValues], async (err, result2) => {
                            if (err) throw err;
                            res
                                .status(200)
                                .send({
                                    success: true,
                                    msg: "Vacancy added successfully.",
                                    result: result1,
                                });
                        });
                    });
                });
            });
        } catch (err) {
            res.status(400).send(err);
            console.log(err);
        }
    },

    updatevacancy: async (req, res) => {
        try {
            let sql =
                "UPDATE vacancy SET TITLE = '" +
                req.body.title +
                "' , COMPANY = '" +
                req.body.company +
                "', CITY = '" +
                req.body.city +
                "' WHERE V_ID='" +
                req.body.id +
                "'  ";
            await _db.query(sql, function (err, result) {
                if (err) throw err;
                res
                    .status(200)
                    .send({ success: true, msg: "Vacancy updated successfully." });
            });
        } catch (err) {
            res.status(400).send(err);
            console.log(err);
        }
    },

    deletevacancy: async (req, res) => {
        try {
            let sql = "DELETE FROM vacancy WHERE V_ID = '" + req.body.id + "'";
            await _db.query(sql, function (err, fields) {
                if (err) throw err;
                res
                    .status(200)
                    .send({ success: true, msg: "Vacancy deleted Succefully.." });
            });
        } catch (err) {
            res.status(400).send(err);
        }
    },
    getvacancy: async (req, res) => {
        try {
            let sql =
                "SELECT v.* FROM vacancy as v WHERE v.V_ID = '" + req.body.id + "'";
            let sql2 =
                "SELECT v.* FROM questions as v WHERE v.V_ID = '" + req.body.id + "'";
            let sql3 =
                "SELECT v.* FROM skills_selected as v WHERE v.V_ID = '" +
                req.body.id +
                "'";

            let questions = [];
            let skills = [];
            await _db.query(sql2, function (err, result2) {
                if (err) throw err;

                questions = result2;
            });
            await _db.query(sql3, function (err, result3) {
                if (err) throw err;

                skills = result3;
            });
            await _db.query(sql, function (err, userDoc) {
                if (err) throw err;

                res
                    .status(200)
                    .send({
                        success: true,
                        data: userDoc,
                        questionsSet: questions,
                        skillsSet: skills,
                    });
            });
        } catch (err) {
            res.status(400).send(err);
        }
    },

    getallvacancies: async (req, res) => {
        try {
            console.log(req.admin.ID);
            let sql =
                "SELECT v.*,(select count(*) from vacancy_candidate where VAC_ID=v.V_ID ) as CANDIDATES FROM vacancy as v INNER JOIN users as u on u.ID= '" +
                req.admin.ID +
                "' INNER JOIN recruiters as r on r.USER_ID=u.ID where v.R_ID=r.ID ORDER BY v.CREATED_AT DESC";
            _db.query(sql, function (err, userDoc, fields) {
                if (err) throw err;
                res.status(200).send({ success: true, data: userDoc });
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    getquestions: async (req, res) => {
        try {
            let sql =
                "SELECT * FROM questions WHERE V_ID = '" + req.body.vacancy_id + "'";
            await _db.query(sql, function (err, userDoc) {
                if (err) throw err;
                res.status(200).send({ success: true, data: userDoc });
            });
        } catch (err) {
            res.status(400).send(err);
        }
    },
    deletequestion: async (req, res) => {
        try {
            let sql = "DELETE FROM questions WHERE ID = '" + req.body.id + "'";
            await _db.query(sql, function (err, fields) {
                if (err) throw err;
                res
                    .status(200)
                    .send({ success: true, msg: "Question deleted Succefully.." });
            });
        } catch (err) {
            res.status(400).send(err);
        }
    },

    addquestion: async (req, res) => {
        try {
            req.body.question.forEach(async (ques) => {
                let sql = "INSERT INTO questions (V_ID, QUESTION, Status) VALUES ?";
                let values = Array([req.body.vacancy_id, ques.question, ques.status]);

                console.log("ques: ", values);

                await _db.query(sql, [values], function (err, result) {
                    if (err) throw err;
                });
            });

            res
                .status(200)
                .send({ success: true, msg: "Question added successfully." });
        } catch (err) {
            res.status(400).send(err);
            console.log(err);
        }
    },

    getquestionanswer: async (req, res) => {
        try {
            let sql =
                "SELECT q.*, qa.*,c.NAME FROM questions as q INNER JOIN question_answer as qa on qa.Q_ID=q.ID INNER JOIN candidates as c on c.LINK='" +
                req.body.id +
                "' WHERE qa.C_ID = c.C_ID";
            let count = 0;
            let score = [];
            let skillsName;

            await _db.query(sql, function (err, userDoc) {
                if (err) throw err;

                if (userDoc.length > 1) {
                    userDoc.forEach(async (elem) => {
                        let getQID =
                            "SELECT area_id FROM area_question WHERE question_id IN (SELECT id from video_question where question = ?)";

                        await _db.query(
                            getQID,
                            [elem.QUESTION],
                            async (err, videoResult) => {
                                if (err) throw err;

                                if (videoResult.length > 0) {
                                    let skills =
                                        "SELECT skill.name FROM skill_area RIGHT JOIN skill ON skill_area.skill_id = skill.id WHERE skill_area.area_id = ?";
                                    await _db.query(
                                        skills,
                                        [videoResult[0].area_id],
                                        (err, skillRes) => {
                                            if (err) throw err;

                                            skillsName = skillRes[0].name;
                                        }
                                    );
                                    let keywordSql =
                                        "SELECT area_keyphrase.area_id, area_keyphrase.keyword_id, keyphrase.phrase FROM area_keyphrase INNER JOIN keyphrase WHERE area_keyphrase.keyword_id = keyphrase.id AND area_keyphrase.area_id = ?";
                                    await _db.query(
                                        keywordSql,
                                        [videoResult[0].area_id],
                                        async (err, keywordRes) => {
                                            if (err) throw err;

                                            count = 0;
                                            const keywordArray = [];
                                            keywordRes.forEach((keyphrase) => {
                                                keywordArray.push(keyphrase.phrase);
                                            });
                                            let text = elem.TEXT_ANSWER;
                                            keywordArray.forEach((txt) => {
                                                if (text.search(txt) > -1) {
                                                    count++;
                                                }
                                            });
                                            let addScore = {
                                                skills_name: skillsName,
                                                count: count,
                                                area_id: videoResult[0].area_id,
                                            };
                                            score.push(addScore);
                                        }
                                    );
                                }
                            }
                        );
                    });
                }

                setTimeout(() => {
                    res.status(200).send({ success: true, data: userDoc, scores: score });
                }, 1000);
            });
        } catch (err) {
            res.status(400).send(err);
        }
    },

    recruitersignup: async (req, res) => {
        try {
            let sql =
                "SELECT * FROM users WHERE USERNAME = '" + req.body.username + "'";
            await _db.query(sql, function (err, userDoc) {
                if (err) throw err;
                if (userDoc != "") {
                    res
                        .status(200)
                        .send({ success: false, msg: "Username is not available." });
                } else {
                    try {
                        console.log("pass: ", req.body);
                        var password = utils.hashPassword(req.body.password);
                        let sql =
                            "INSERT INTO users (ROLE,FULLNAME,EMAIL,USERNAME,PASSWORD,STATUS,CREATED_AT) VALUES ?";
                        let values = Array([
                            "2",
                            req.body.fullname,
                            req.body.email,
                            req.body.username,
                            password,
                            "1",
                            new Date().getTime(),
                        ]);
                        _db.query(sql, [values], function (err, result) {
                            if (err) throw err;
                            let sql2 =
                                "INSERT INTO recruiters (USER_ID,PHONE,COMPANY,CITY,STATUS,CREATED_AT) VALUES ?";
                            let values2 = Array([
                                result.insertId,
                                req.body.phone,
                                req.body.company,
                                req.body.city,
                                "1",
                                new Date().getTime(),
                            ]);
                            _db.query(sql2, [values2], function (err, result2) {
                                if (err) throw err;
                                async.auto(
                                    {
                                        findEntity: (cb) => {
                                            let sql =
                                                "SELECT * FROM users WHERE USERNAME ='" +
                                                req.body.username +
                                                "' and ROLE=2";
                                            _db.query(sql, function (err, userDoc, fields) {
                                                if (err) throw err;

                                                return cb(null, userDoc[0]);
                                            });
                                        },
                                        generateToken: [
                                            "findEntity",
                                            function (results, cb) {
                                                let accessToken = jwt.sign(
                                                    {
                                                        userId: results.findEntity.ID,
                                                        userRole: results.findEntity.ROLE,
                                                    },
                                                    process.env.TOKEN_SECRET,
                                                    { expiresIn: "1h" }
                                                );

                                                return cb(null, accessToken);
                                            },
                                        ],
                                        updateToken: [
                                            "generateToken",
                                            function (results, cb) {
                                                let updateSql =
                                                    "UPDATE users SET TOKEN = '" +
                                                    results.generateToken +
                                                    "' WHERE ID = '" +
                                                    results.findEntity.ID +
                                                    "'";
                                                _db.query(updateSql, function (err, result) {
                                                    if (err) throw err;
                                                    return cb(null, result);
                                                });
                                            },
                                        ],
                                    },
                                    (err, results) => {
                                        if (err) return res.status(400).send(err);
                                        return res
                                            .status(200)
                                            .send({
                                                success: true,
                                                msg: "Recruiter added successfully.",
                                                recruiter: results.findEntity.USERNAME,
                                                uid: results.findEntity.ID,
                                                token: results.generateToken,
                                            });
                                    }
                                );
                                // res.status(200).send({ success: true, msg: 'Recruiter added successfully.' });
                            });
                        });
                    } catch (err) {
                        res.status(400).send(err);
                        console.log(err);
                    }
                }
            });
        } catch (err) {
            res.status(400).send(err);
        }
    },

    getQuestionFromSkills: async (req, res) => {
        try {
            let noOfSkills = req.body.noOfSkills;
            let areaSql =
                "SELECT skill_area.skill_id,area_question.area_id,area_question.question_id FROM `skill_area` RIGHT JOIN area_question ON area_question.area_id=skill_area.area_id WHERE skill_id=? and area_question.area_id ORDER BY rand() LIMIT ?";
            let skillsSetArray = [];
            for (let i = 1; i <= noOfSkills; i++) {
                let j = 1;
                if (i < 4) {
                    j = 2;
                } else {
                    j = 1;
                }
                _db.query(areaSql, [i, j], function (err, result) {
                    if (err) throw err;
                    console.log(result);
                    skillsSetArray.push(result);
                });
            }
            setTimeout(() => {
                res.status(200).send({ success: true, data: skillsSetArray });
            }, 1000);
        } catch (error) {
            res.status(400).send(error);
        }
    },

    getQuestionFromId: async (req, res) => {
        try {
            let questionid = req.body.quesId;

            let sql = `select question from video_question where id in (${questionid})`;
            _db.query(sql, function (err, result) {
                if (err) throw err;
                console.log(result);
                res.status(200).send({ success: true, question: result });
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    getcomments: async (req, res) => {
        try {
            let area_id = req.body.areaId;
            let score_id = req.body.scoreId;

            let sql = `SELECT comment.id, comment.name FROM area_comment INNER JOIN comment on area_comment.comment_id = comment.id where area_comment.area_id= ${area_id} AND area_comment.score_id = ${score_id}`;
            _db.query(sql, function (err, result) {
                if (err) throw err;

                res.status(200).send({ success: true, comments: result });
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    getAddedComments: async (req, res) => {
        try {
            let sql = `Select * from question_answer_comments where V_Id = ${req.body.V_ID} and C_ID = ${req.body.C_ID}`;
            _db.query(sql, function (err, result) {
                if (err) throw err;

                res.status(200).send({ success: true, comments: result });
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },

    addcomments: async (req, res) => {
        try {
            let sql = `Insert into question_answer_comments set V_ID = ${req.body.V_ID}, C_ID = ${req.body.C_ID}, COMMENT = "${req.body.COMMENT}"`;
            _db.query(sql, function (err, result) {
                if (err) throw err;

                res.status(200).send({ success: true, comments: result });
            });
        } catch (error) {
            res.status(400).send(error);
        }
    },
};

// SELECT users.ID,useraddress.USER_ID, users.FULLNAME,useraddress.ADDRESS_TYPE, useraddress.ADDRESS, useraddress.CITY, useraddress.STATE, useraddress.PIN from useraddress,users WHERE users.ID = useraddress.USER_ID
