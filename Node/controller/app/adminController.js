const async = require('async')
const _db = require('../../database/db_connect')
const utils = require('../../services/utils')
const jwt = require('jsonwebtoken');





module.exports = {
  


    login: (req, res) => {
        if (!req.body.username || !req.body.password)
            return res.status(400).send({ msg: 'Required field is missing' })
        async.auto({
            findEntity: (cb) => {
                let sql = "SELECT * FROM users WHERE USERNAME ='" + req.body.username + "' and ROLE=1"
                _db.query(sql, function (err, userDoc, fields) {
                    if (err) throw err
                    if (!userDoc[0])
                        return cb({ msg: 'Username does not exist' })
                    if (!utils.comparePassword(req.body.password, userDoc[0].PASSWORD))
                        return cb({ msg: 'Invalid password' })
                    return cb(null, userDoc[0])
                })
            },
            generateToken: ['findEntity', function (results, cb) {
                let accessToken = jwt.sign({
                    userId: results.findEntity.ID,
                    userRole: results.findEntity.ROLE
                    
                }, process.env.TOKEN_SECRET, { expiresIn: '1h' })

                return cb(null, accessToken)
            }],
            updateToken: ['generateToken', function (results, cb) {
                let updateSql = "UPDATE users SET TOKEN = '" + results.generateToken + "' WHERE ID = '" + results.findEntity.ID + "'"
                _db.query(updateSql, function (err, result) {
                    if (err) throw err
                    return cb(null, result)
                })
            }]
        }, (err, results) => {
            if (err)
                return res.status(400).send(err)
            return res.status(200).send({ success: true, admin: results.findEntity.USERNAME, token: results.generateToken })
        })
    },//End of Login Function

    getallcities: async (req , res) => {
        try {
            let sql = "SELECT * FROM city WHERE STATUS=1";
            _db.query(sql, function (err, userDoc, fields) {
                if (err) throw err
                res.status(200).send({ success: true, data: userDoc })
            })
        } catch (error) {
            res.status(400).send(error)
        }
    },
	
    addrecruiter: async (req, res) => {
        try {
            let sql = "SELECT * FROM users WHERE USERNAME = '" + req.body.username + "'";
            await _db.query(sql, function (err, userDoc) {
                if (err) throw err
                if(userDoc!=''){
                    res.status(200).send({ success: false, msg: 'Username is not available.' });  
                } else {
                    try {

                        var password = utils.hashPassword(req.body.password);
                        let sql = "INSERT INTO users (ROLE,FULLNAME,EMAIL,USERNAME,PASSWORD,STATUS,CREATED_AT) VALUES ?";
                        let values = Array([
                            '2',
                            req.body.fullname,
                            req.body.email,
                            req.body.username,
                            password,
                            '1',
                            new Date().getTime()
                        ])
                         _db.query(sql, [values], function (err, result) {
                            if (err) throw err
                            let sql2 = "INSERT INTO recruiters (USER_ID,PHONE,COMPANY,CITY,STATUS,CREATED_AT) VALUES ?";
                            let values2 = Array([
                                result.insertId,
                                req.body.phone,
                                req.body.company,
                                req.body.city,
                                '1',
                                new Date().getTime()
                            ])
                             _db.query(sql2, [values2], function (err, result2) {
                                if (err) throw err
                                res.status(200).send({ success: true, msg: 'Recruiter added successfully.' });
                            })    
                            
                        })

                    } catch (err) {
                        res.status(400).send(err)
                        console.log(err);
                    }


                }


            })
        } catch (err) {
            res.status(400).send(err)
        }

    },


    updaterecruiter: async (req, res) => {
        try {
            let sql = "UPDATE recruiters SET PHONE = '" + req.body.phone + "' , COMPANY = '" + req.body.company + "', CITY = '" + req.body.city + "' WHERE USER_ID='" + req.body.id + "'  ";
            await _db.query(sql, function (err, result) {
                if (err) throw err
                let sql = "UPDATE users SET FULLNAME = '" + req.body.fullname + "' , EMAIL = '" + req.body.email + "', STATUS = '" + req.body.status + "' WHERE ID='" + req.body.id + "'  ";
                 _db.query(sql, function (err, result) {
                    if (err) throw err
                    res.status(200).send({ success: true, msg: 'Recruiter updated successfully.' });
                })
                
            })
        } catch (err) {
            res.status(400).send(err)
            console.log(err);
        }
    },
   

    deleterecruiter: async (req, res) => {
        try {
            let sql = "DELETE FROM recruiters WHERE USER_ID = '" + req.body.id + "'";
            await _db.query(sql, function (err, fields) {
                if (err) throw err
                let sql = "DELETE FROM users WHERE ID = '" + req.body.id + "'";
                _db.query(sql, function (err, fields) {
                    if (err) throw err
                    res.status(200).send({ success: true, msg: 'Recruiter deleted Succefully..' });
                })
            })
        } catch (err) {
            res.status(400).send(err)

        }
    },
   
    getrecruiter: async (req, res) => {
        try {
            let sql = "SELECT u.*, r.PHONE, r.COMPANY, r.CITY FROM users as u left join recruiters as r on r.USER_ID=u.ID  WHERE u.ID = '" + req.body.id + "'";
            await _db.query(sql, function (err, userDoc) {
                if (err) throw err
                res.status(200).send({ success: true, data: userDoc });
            })
        } catch (err) {
            res.status(400).send(err)
        }
    },

    getallrecruiters: async (req , res) => {
        try {
            let sql = "SELECT u.*,r.CITY, r.PHONE, r.COMPANY FROM users as u left join recruiters as r on r.USER_ID=u.ID Where u.ROLE=2";
            _db.query(sql, function (err, userDoc, fields) {
                if (err) throw err
                res.status(200).send({ success: true, data: userDoc })
            })
        } catch (error) {
            res.status(400).send(error)
        }
    },    
    
    
}

