const async = require('async')
const _db = require('../../database/db_connect')
const utils = require('../../services/utils')
const jwt = require('jsonwebtoken');
const multer = require('multer');
const converterAI = require('../../speech-to-text-conversion');
const keywordExtract = require('../../keyword-extract');
const { json } = require('body-parser');
// import convertSpeechToText from '../../speech-to-text-conversion'


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './question-answer');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});

var upload = multer({ storage: storage }).single("video");


module.exports = {


    questions: async (req, res) => {
        try {
            let sql = "SELECT q.* FROM vacancy as v inner join questions as q on q.V_ID=v.V_ID and v.LINK='" + req.body.code + "'";
            await _db.query(sql, function (err, userDoc) {
                if (err) throw err
                res.status(200).send({ success: true, data: userDoc });
            })
        } catch (err) {
            res.status(400).send(err)
        }
    },






    // checkDetails: async (req, res) => {

    //     try {
    //         let sql = "SELECT * FROM candidates where EMAIL='" + req.body.email + "'";

    //        // let sql = "SELECT vc.* FROM vacancy_candidate as vc , candidates as c, vacancy as v where vc.CAN_ID=c.C_ID and vc.VAC_ID=v.V_ID and v.LINK='" + req.body.link + "' and c.EMAIL='" + req.body.email + "'";
    //         await _db.query(sql, function (err, userDoc) {
    //              console.log(userDoc)
    //             if (err) throw err
    //             if (!userDoc)
    //                 return res.status(200).send({ success: false, message: "Invalid vacancy" })
    //             return res.status(200).send({ success: true, data: userDoc })    
    //         })
    //     } catch (err) {
    //         res.status(400).send(err)
    //     }

    // },//End of Login Function



    checkDetails: (req, res) => {
        console.log(req.body)
        if (!req.body.email)
            return res.status(400).send({ msg: 'Required field is missing' })
        async.auto({
            findEntity: (cb) => {

                sql = "SELECT * FROM vacancy WHERE LINK ='" + req.body.code + "'"
                _db.query(sql, function (err, vacDoc, fields) {
                    if (err) throw err
                    if (vacDoc[0]) {
                        let sql = "INSERT INTO candidates (NAME, JOB_TITLE, COMPANY, EMAIL, LINK, CREATED_AT) VALUES ?";
                        let values = Array([
                            req.body.name,
                            vacDoc[0].TITLE,
                            vacDoc[0].COMPANY,
                            req.body.email,
                            utils.getRandomCode(25),
                            new Date().getTime(),
                        ])
                        _db.query(sql, [values], function (err, result) {
                            if (err) throw err
                            sqlc = "SELECT * FROM candidates WHERE EMAIL ='" + req.body.email + "'"
                            _db.query(sqlc, function (err, userDoc, fields) {
                                if (err) throw err
                                if (!userDoc[0])
                                    return cb({ msg: 'Email does not exist' })
                                return cb(null, userDoc[0])
                            })

                        })
                    }

                })


            },
            findVacancy: ['findEntity', function (results, cb) {
                let sql = "SELECT * FROM vacancy WHERE LINK ='" + req.body.code + "'"
                _db.query(sql, function (err, userVac, fields) {
                    if (err) throw err
                    if (!userVac[0])
                        return cb({ msg: 'Invalid vacancy' })
                    return cb(null, userVac[0])
                })
            }],
            generateToken: ['findEntity', function (results, cb) {
                console.log("rrg", results.findEntity)
                let accessToken = jwt.sign({
                    candidateId: results.findEntity.C_ID
                }, process.env.TOKEN_SECRET, { expiresIn: '1h' })
                return cb(null, accessToken)
            }],
            updateToken: ['generateToken', 'findVacancy', 'findEntity', function (results, cb) {
                let sql = "INSERT INTO vacancy_candidate (VAC_ID, CAN_ID) VALUES ?";
                let values = Array([
                    results.findVacancy.V_ID,
                    results.findEntity.C_ID
                ])
                _db.query(sql, [values], function (err, result) {
                    if (err) throw err
                })
                let updateSql = "UPDATE candidates SET TOKEN = '" + results.generateToken + "' WHERE C_ID = '" + results.findEntity.C_ID + "'"
                _db.query(updateSql, function (err, result) {
                    if (err) throw err
                    return cb(null, result)
                })
            }]
        }, (err, results) => {
            if (err)
                return res.status(400).send(err)
            return res.status(200).send({ success: true, v_id: results.findVacancy.V_ID, c_id: results.findEntity.C_ID, c_name: results.findEntity.NAME, token: results.generateToken })
        })
    },



    // questionAnswer: (req, res) => {
    //     upload(req, res, (err) => {
    //         if (err) {
    //             res.status(400).send("Something went wrong!");
    //         }
    //         res.send(req.file);
    //     })
    // },


    questionAnswer: async (req, res) => {
        try {
            let sql = "SELECT * FROM question_answer WHERE V_ID ='" + req.body.vacancy_id + "' AND C_ID ='" + req.body.candidate_id + "' AND Q_ID='" + req.body.question_id + "'";
            var converted;
            const convertedArr = []
            await _db.query(sql, async (err, result) => {

                if (result.length > 0) {
                    let updateSql = "UPDATE question_answer SET ANSWER = '" + req.body.answer + "' WHERE ID = '" + result[0].ID + "'";
                    await _db.query(updateSql, async function (err, result2) {
                        if (err) throw err
                        converterAI.convertSpeechToText(req.body.answer).then(async (getConvertedText) => {
                            // console.log('done val: ',done)
                            let getText = JSON.parse(getConvertedText);
                            if (getText.results.length > 0) {
                                for(getres in getText.results){
                                    converted = getText.results[getres].alternatives[getres].transcript
                                    console.log('get res: ', converted)
                                    convertedArr.push(converted)
                                }
                            }
                            console.log("converted text: ", convertedArr)

                            var extractedkeyword = keywordExtract.extractor(converted,['problem solving','adaptability','time management','teamwork','leadership','communication'])
                            console.log('extractedkeyword: ', extractedkeyword)
                            let addTextSql = "UPDATE question_answer SET TEXT_ANSWER = '" + converted + "' WHERE ID = '" + result[0].ID + "'";
                            await _db.query(addTextSql, function (err, res) {
                                if (err) throw err
                                console.log("response: ", res)
                            })

                        }).catch((err) => {
                            console.log('err: ', err)
                        })
                    })
                } else {
                    converterAI.convertSpeechToText(req.body.answer).then(async (getConvertedText) => {
                        // console.log('done val: ',done)
                        let getText = JSON.parse(getConvertedText);

                        if (getText.results.length > 0) {
                            for(getres in getText.results){
                                converted = getText.results[getres].alternatives[getres].transcript
                                console.log('get res: ', converted)
                                convertedArr.push(converted)
                            }
                        }
                        console.log(" new converted: ", converted)
                        var extractedkeyword = keywordExtract.extractor(converted,['one','problem solving','adaptability','time management','teamwork','leadership','communication'])
                        console.log('extractedkeyword: ', extractedkeyword)

                        let sql = "INSERT INTO question_answer (V_ID, C_ID, Q_ID, ANSWER, TEXT_ANSWER) VALUES ?";
                        let values = Array([
                            req.body.vacancy_id,
                            req.body.candidate_id,
                            req.body.question_id,
                            req.body.answer,
                            converted
                        ])
                        await _db.query(sql, [values], async function (err, result) {
                            if (err) throw err

                        })

                    }).catch((err) => {
                        console.log('err: ', err)
                    })

                }

            });
            res.status(200).send({ success: true, msg: 'Answer send successfully.' });

        } catch (err) {
            res.status(400).send(err)
            console.log(err);
        }
    },


    answerFiles: async (req, res) => {
        try {
            // let converted = {}
            let path = req.file.path

            res.status(200).send({ "status": true, "filePath": path })

            // var converted = convertSpeechToText(path)
        } catch (error) {

        }
    }

}

