const express = require('express')
const bodyParser = require('body-parser')
const candidateRouter = express.Router()
const querystring = require('querystring');
// bodyParser.urlencoded({ extended: true })
const multer = require('multer')

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});

const upload = multer({ storage: storage })

const isAuthenticated = require('../../services/isAuthenticated')
const candidateController = require('../../controller/app/candidateController')
const jsonParser = bodyParser.json()
const urlencoded = bodyParser.urlencoded({ extended: true })



candidateRouter.post('/questionsByLink', jsonParser, candidateController.questions)
candidateRouter.post('/checkdetails', jsonParser, candidateController.checkDetails)
candidateRouter.post('/questionanswer', jsonParser, candidateController.questionAnswer)
candidateRouter.post('/answerFiles', upload.single("file"), candidateController.answerFiles)
module.exports = candidateRouter