const express = require('express')
const bodyParser = require('body-parser')
const recruiterRouter = express.Router()
const querystring = require('querystring');
// bodyParser.urlencoded({ extended: true })


const isAuthenticated = require('../../services/isAuthenticated')
const recruiterController = require('../../controller/app/recruiterController')
const jsonParser = bodyParser.json()
 const urlencoded = bodyParser.urlencoded({ extended: true })



 recruiterRouter.post('/login', jsonParser, recruiterController.login)
 recruiterRouter.post('/recruitersignup', jsonParser, recruiterController.recruitersignup)
 recruiterRouter.post('/verfiyEmailVerificationCode/:id/:token', jsonParser, recruiterController.verfiyEmailVerificationCode)
 recruiterRouter.post('/sendEmailVerificationCode', jsonParser, recruiterController.sendEmailVerificationCode)
 recruiterRouter.get('/getusers', isAuthenticated, jsonParser, recruiterController.getUsersDetail)
 recruiterRouter.get('/getcities', isAuthenticated, jsonParser, recruiterController.getallcities)
 recruiterRouter.get('/getskills', isAuthenticated, jsonParser, recruiterController.getskills)
 recruiterRouter.get('/getscores', isAuthenticated, jsonParser, recruiterController.getscores)
 recruiterRouter.post('/getquestionfromskills', isAuthenticated, jsonParser, recruiterController.getQuestionFromSkills)
 recruiterRouter.post('/getquestionfromid', isAuthenticated, jsonParser, recruiterController.getQuestionFromId)
 recruiterRouter.post('/addvacancy', isAuthenticated, jsonParser, recruiterController.addvacancy)
 recruiterRouter.post('/updatevacancy', isAuthenticated, jsonParser, recruiterController.updatevacancy)
 recruiterRouter.post('/deletevacancy', isAuthenticated, jsonParser, recruiterController.deletevacancy)
 recruiterRouter.post('/getvacancy', isAuthenticated, jsonParser, recruiterController.getvacancy)
 recruiterRouter.get('/getallvacancies', isAuthenticated, jsonParser, recruiterController.getallvacancies)

 recruiterRouter.post('/addcandidate', isAuthenticated, jsonParser, recruiterController.addcandidate)
 recruiterRouter.post('/updatecandidate', isAuthenticated, jsonParser, recruiterController.updatecandidate)
 recruiterRouter.post('/deletecandidate', isAuthenticated, jsonParser, recruiterController.deletecandidate)
 recruiterRouter.post('/getcandidate', isAuthenticated, jsonParser, recruiterController.getcandidate)
 recruiterRouter.get('/getallcandidates', isAuthenticated, jsonParser, recruiterController.getallcandidates)

 recruiterRouter.post('/getcomments', isAuthenticated, jsonParser, recruiterController.getcomments)
 recruiterRouter.post('/getquestions', isAuthenticated, jsonParser, recruiterController.getquestions)
 recruiterRouter.post('/deletequestion', isAuthenticated, jsonParser, recruiterController.deletequestion)
 recruiterRouter.post('/addquestion', isAuthenticated, jsonParser, recruiterController.addquestion)
 recruiterRouter.post('/interviewanswer', jsonParser, recruiterController.getquestionanswer)
 recruiterRouter.post('/getaddedcomments', jsonParser, recruiterController.getAddedComments)
 recruiterRouter.post('/addcomments', jsonParser, recruiterController.addcomments)

module.exports = recruiterRouter