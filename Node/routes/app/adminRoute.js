const express = require('express')
const bodyParser = require('body-parser')
const adminRouter = express.Router()
const querystring = require('querystring');
// bodyParser.urlencoded({ extended: true })


const isAuthenticated = require('../../services/isAuthenticated')
const adminController = require('../../controller/app/adminController')
const jsonParser = bodyParser.json()
 const urlencoded = bodyParser.urlencoded({ extended: true })



adminRouter.post('/login', jsonParser, adminController.login)
adminRouter.get('/getcities', isAuthenticated, jsonParser, adminController.getallcities)
adminRouter.post('/addrecruiter', isAuthenticated, jsonParser, adminController.addrecruiter)
adminRouter.post('/updaterecruiter', isAuthenticated, jsonParser, adminController.updaterecruiter)
adminRouter.post('/deleterecruiter', isAuthenticated, jsonParser, adminController.deleterecruiter)
adminRouter.post('/getrecruiter', isAuthenticated, jsonParser, adminController.getrecruiter)
adminRouter.get('/getallrecruiters', isAuthenticated, jsonParser, adminController.getallrecruiters)
module.exports = adminRouter