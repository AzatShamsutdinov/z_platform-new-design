var async = require('async');
var jwt = require('jsonwebtoken');
var _ = require('underscore');
const dotenv = require('dotenv')
const _db = require('../database/db_connect')

dotenv.config()

var tokenToEntityMap = {
    atoken: {
        type: 'admin'
    }
}

module.exports = function (req, res, next) {
    var bearerHeader = req.headers['authorization']
    //console.log(bearerHeader)
    // Split at the space
    var bearer = bearerHeader.split(' ');
    // Get token from array
    var bearerToken = bearer[1];
    // Set the token
    var token = bearerToken;

    var entity = tokenToEntityMap['atoken'];


    async.auto({
        validateToken: function (cb) {
            jwt.verify(token, process.env.TOKEN_SECRET, function (err, payload) {
                if (err)
                    return res.status(401).send(err.message);
                return cb(null, payload);
            })
        },
        entity: ['validateToken', function (results, cb) {

            let sql = "SELECT * FROM users WHERE ID ='" + results.validateToken.userId + "' and ROLE='" + results.validateToken.userRole + "'"
            _db.query(sql, function (err, matchedEntity, fields) {
                if (err) throw err

                if (!matchedEntity[0])
                    return cb({ msg: 'Invalid user' })

                if (matchedEntity[0].TOKEN !== token)
                    return cb({ msg: 'Could not be authorized' });

                var keysToPick = ['ID', 'EMAIL','ROLE'];
                req[entity.type] = _.pick(matchedEntity[0], keysToPick);
                return cb(null, matchedEntity[0]);
            })
        }]
    }, function (err, results) {
        if (err)
            return res.status(401).send({ msg: 'Could not be authorized' })
        return next();
    });

};